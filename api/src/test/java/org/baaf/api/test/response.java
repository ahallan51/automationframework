package org.baaf.api.test;

import org.testng.annotations.Test;//TestNg import file
import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class response {

    @Test
    public void WeatherDetails() {
        baseURI = "https://reqres.in/api";
        RequestSpecification request = given();

        // Make a GET request call directly by using RequestSpecification.get() method.
        // Make sure you specify the resource name.
        Response response = request.get("/users?page=2");
        response.getBody();
        System.out.println("Response Body is: " + response.asString());
        response.body();

        // Response.asString method will directly return the content of the body
        // as String.
        //System.out.println("Response Body is =>  " + response.asString());
    }
}
