package com.block8.framework.repositories.test;

import java.io.IOException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.block8.framework.repositories.ObjectMapper;
import com.block8.uiengine.UIExecute;

@Test
public class ObjectMapperTest {

  public ObjectMapperTest() throws Exception {}

  ObjectMapper omRepository;

  @BeforeTest
  public void beforeTest() {

    String ObjectRepositoryPath =
    UIExecute.strRepositories + "PlayChipCentralRepo.txt";
    try {
      omRepository = new ObjectMapper(ObjectRepositoryPath);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Test
  public void f() {
    System.out.println(omRepository.getElementLocator("Login.Username").toString());
  }
}
