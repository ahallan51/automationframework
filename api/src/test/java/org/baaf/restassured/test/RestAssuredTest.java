package org.baaf.restassured.test;

import org.baaf.TestNGFormatter;
import org.baaf.configurations.TestSetup;
import org.baaf.restassured.RAHelper;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Test
@Listeners(TestNGFormatter.class)
public class RestAssuredTest extends TestSetup {

    @Test
    public void testAPI(){
        RAHelper ra= new RAHelper(null);
        for (String methodName : listOfAPIs) {
            initiateTestCase(methodName);
            ra.execute(methodName);
        }
    }

    @Test
    public void testAPIError(){
        RAHelper ra= new RAHelper(null);
        Assert.assertEquals(ra.validate(URIString),"Pass","Testing failed for API" );
    }
}