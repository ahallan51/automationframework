package org.baaf.api;

import io.restassured.response.Response;

public class APIResponse {
    Response rAPIResponse;
    APIMethods amAPIMethod;
    int iHTTPSATUSCODE=0;

    public APIResponse(APIMethods amAPIMethods){
        this.amAPIMethod = amAPIMethods;
    }

	public void setResponse(Response rResponse) {
        this.rAPIResponse = rResponse;
        this.iHTTPSATUSCODE = rResponse.getStatusCode();
    }

    public String readValue(String strJSONPath){
        return rAPIResponse.path(strJSONPath).toString();
    }

    public int getHTTPSATUSCODE() {
        return iHTTPSATUSCODE;
    }
    
    @Override
    public String toString(){
    	rAPIResponse.prettyPrint();
        return rAPIResponse.asString();
    }
}