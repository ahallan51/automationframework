package com.block8.framework.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class PropertiesReader implements DataFeeder {

  Properties properties = new Properties();
  FileInputStream fileInput = null;
  HashMap<String, String> hmPropertiesData = new HashMap<String, String>();

  private PropertiesReader(String strFilePath) throws IOException {
    readProperties(strFilePath);
  }

  @Override
  public String isValid() {
    // TODO check if the file is valid
    return null;
  }

  @Override
  public boolean isEmpty() {
    // TODO check if the file is empty
    return false;
  }

  @Override
  public String getData(String strFieldName) {
    // TODO Auto-generated method stub
    return properties.getProperty(strFieldName);
  }

  public static PropertiesReader getInstance(String strFilePath) throws IOException {
    PropertiesReader prDataFeed = new PropertiesReader(strFilePath);
    return prDataFeed;
  }

  public Properties getRepositoryObject() {
    return properties;
  }

  public void readProperties(String strFilePath) throws IOException {
    properties.clear();
    try {
      fileInput = new FileInputStream(new File(strFilePath));
      properties.load(fileInput);
      fileInput.close();
    } catch (IOException e) {
      throw new IOException(
          "There was an error opening properties file - " + strFilePath + "\n" + e.getMessage(), e);
    }
  }
}
