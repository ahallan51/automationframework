package com.block8.framework.data;

public interface DataFeeder {

  public String isValid();

  public boolean isEmpty();

  public String getData(String strColumnName);
}
