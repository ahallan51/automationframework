package org.baaf.api;

import java.util.HashMap;

public interface IAPI {
    
    public void setAPIMethod(APIMethods amAPIMethod);
    public void prepareAPI(APIHeaders ahAPIHeader,HashMap<String,String> erDataSet);
    public void setBaseURL(String strURL);
    public void setURI(String strURIPath);
    public APIResponse execute();
    public String getAPIURI();
}