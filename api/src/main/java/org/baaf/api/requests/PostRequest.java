package org.baaf.api.requests;

import static io.restassured.config.EncoderConfig.encoderConfig;
import java.util.HashMap;
import org.baaf.api.APIHeaders;
import org.baaf.api.APIMethods;
import org.baaf.api.APIResponse;
import org.baaf.api.sharables.Shareable;
import org.baaf.utilities.json.JsonHelper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PostRequest extends APIRequest {

    String strPayLoad;
    
    public PostRequest(APIMethods amMethod, JsonHelper jsAPIDetails, Shareable shVariables) {
        super(amMethod, jsAPIDetails,shVariables);
    }

    @Override
    public APIResponse execute() {
        APIResponse arAPIResponse = new APIResponse(amMethod);
        arAPIResponse.setResponse(requestSpecs.post(strAPIPath));
        return arAPIResponse;
    }

    @Override
    public void prepareAPI(APIHeaders ahAPIHeader, HashMap<String, String> erDataSet) {
        strAPIPath = updateURIParams(strAPIPath, erDataSet);
        requestSpecs.headers(createHeaders(erDataSet).getHeaderMap());
        strPayLoad = createRequestPayLoad(erDataSet);
        System.out.println("Payload \n --" + strPayLoad );
        requestSpecs.config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("application/x-amz-json-1.1", ContentType.TEXT)));
        requestSpecs.body(strPayLoad);
    }
}