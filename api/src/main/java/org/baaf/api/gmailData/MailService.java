// package org.baaf.api.gmailData;

// import java.io.IOException;
// import java.io.InputStream;
// import java.io.InputStreamReader;
// import java.security.GeneralSecurityException;
// import java.util.Collections;
// import java.util.List;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import com.google.api.client.auth.oauth2.Credential;
// import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
// import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
// import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
// import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
// import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
// import com.google.api.client.http.javanet.NetHttpTransport;
// import com.google.api.client.json.JsonFactory;
// import com.google.api.client.json.jackson2.JacksonFactory;
// //import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
// import com.google.api.client.util.store.FileDataStoreFactory;
// import com.google.api.services.gmail.Gmail;
// import com.google.api.services.gmail.GmailScopes;
// import com.google.api.services.gmail.model.Label;
// import com.google.api.services.gmail.model.ListLabelsResponse;
// import com.google.api.services.gmail.model.ListMessagesResponse;
// import com.google.api.services.gmail.model.Message;
// import com.google.api.services.gmail.model.MessagePart;
// import com.google.appengine.repackaged.org.apache.commons.codec.binary.Base64;

// public class MailService {

// 	private static final String APPLICATION_NAME = "Gmail API Java Quickstart";
//     private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
//     private static final String TOKENS_DIRECTORY_PATH = "tokens";

//     /**
//      * Global instance of the scopes required by this quick start.
//      * If modifying these scopes, delete your previously saved tokens/ folder.
//      */
//     private static final List<String> SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);
//     private static final String CREDENTIALS_FILE_PATH = "/credentials.json";
//     Gmail service;
//     String user = "me";
    
//     /**
//      * @author Deepak Varna
//      * 
//      * @description Initialize Gmail APIs, generate tokens required.
//      * 
//      * */
//     public MailService() {
//     	NetHttpTransport HTTP_TRANSPORT;
// 		try {
// 			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
// 	        service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
// 	                .setApplicationName(APPLICATION_NAME)
// 	                .build();
// 		} catch (GeneralSecurityException | IOException e) {
// 			e.printStackTrace();
// 		}
//     }
    
//     /**
//      * Creates an authorized Credential object.
//      * @param HTTP_TRANSPORT The network HTTP Transport.
//      * @return An authorized Credential object.
//      * @throws IOException If the credentials.json file cannot be found.
//      */
//     private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
//         // Load client secrets.
//         InputStream in = GmailQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
//         GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

//         GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
//                 HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
//                 .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
//                 .setAccessType("offline")
//                 .build();
//         LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
//         return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
//     }
    
//     /**
//      * Returns Email content in raw format
//      * @return - Base64 encoded email content string
//      * */
//     public String getEmailContent() {
//         ListMessagesResponse listMessages;
//         String strActualContent;
// 		try {
// 			listMessages = service.users().messages().list(user).execute();
// 	        List<Message> messages = listMessages.getMessages();
//         	Message message = messages.get(0);
//         	Message mess = service.users().messages().get(user, message.getId()).setFormat("full").execute();
//         	strActualContent = mess.getPayload().getParts().get(0).toPrettyString();
// 		} catch (IOException e) {
// 			e.printStackTrace();
// 			System.out.println("Error while fetching string from message!");
// 			return null;
// 		}
// 		return strActualContent;
//     }
    
//     public List<Label> getAvailableLabels(){
//     	List<Label> llAvailableLabels;
//     	try {
// 			ListLabelsResponse llrLabels = service.users().labels().list(user).execute();
// 			llAvailableLabels = llrLabels.getLabels();
// 		} catch (IOException e) {
// 			e.printStackTrace();
// 			return null;
// 		}
//     	return llAvailableLabels;
//     }
    
    
//     public List<MessagePart> getLatestMessageContent(String strMessageSubject) throws Exception {
//     	if(strMessageSubject == null) {
//     		throw new Exception("Email Subject cannot be empty.");
//     	}
//     	ListMessagesResponse glMessageRespose = service.users().messages().list(user).setQ("subject:("+ strMessageSubject +")").execute();
//     	List<Message> gmMessages = glMessageRespose.getMessages();
//     	Message gMessage = gmMessages.get(0);
//     	gMessage = service.users().messages().get(user, gMessage.getId()).setFormat("full").execute();
//     	return gMessage.getPayload().getParts();
//     }
    
//     public String getMessageTextContent(String strEmailSubject) {
//     	try {
// 			return getLatestMessageContent(strEmailSubject).get(0).toPrettyString();
// 		} catch (Exception e) {
// 			e.printStackTrace();
// 			return null;
// 		}
//     }
    
//    public String retrieveURL(String strEmailContent) {
//    	Pattern pStartingPattern = Pattern.compile("https*<[/:a-zA-Z0-9]*");
//    	Matcher mURLStart = pStartingPattern.matcher(strEmailContent);  
//    	mURLStart.find();
//    	System.out.println("Region reported:: " + mURLStart.regionStart()+1);
//    	String strFinalURL = mURLStart.group().trim();
//    	return strFinalURL.substring(1, strFinalURL.length()-1);
//    }
   
//    public String getDecodedString(String base64String) {
//        byte[] emailBytes = Base64.decodeBase64(base64String);
//        return new String(emailBytes);
//   }
// }
