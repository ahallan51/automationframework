package org.baaf.api.sharables;

public interface Shareable {

	public final String sharingType = "SharedValue";
	
	public boolean exists(String strName);
	public String get(String strName);
	public boolean set(String strName,String strValue);
}
