package com.block8.framework.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.block8.uiengine.UIExecute;

public class TestBase {
  public WebDriver driver;
  public Properties prop;
  static Logger logger;

  public TestBase() {
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_hhmmss");
      System.setProperty("current.date", dateFormat.format(new Date()));
      prop = new Properties();
      FileInputStream inputStream =
          new FileInputStream(
            UIExecute.strRootPath + "/uia/Reporting/configurations/config.properties");
      prop.load(inputStream);
      PropertyConfigurator.configure(
        UIExecute.strRootPath + "/uia/Reporting/log4j.properties");
      logger = Logger.getLogger(TestBase.class.getName());
    } catch (FileNotFoundException Ex) {
      System.out.println(Ex.getMessage());
      Ex.printStackTrace();
      logger.info("File not found: " + Ex.getMessage());
    } catch (IOException Ex) {
      logger.info("Exception occurred: " + Ex.getMessage());
    }
  }

  public void initialization() throws MalformedURLException {
    String browserName = prop.getProperty("Browser").toLowerCase();
    String environment = prop.getProperty("Environment").toLowerCase();
    switch (environment) {
      case "local":
        switch (browserName) {
          case "chrome":
            logger.info("Starting tests on chrome browser.");
            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            options.addArguments("--disable-extensions");
            driver = new ChromeDriver(options);
            break;

          case "firefox":
            logger.info("Starting tests on firefox browser.");
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            break;

          case "ie":
            logger.info("Starting tests on ie browser.");
            WebDriverManager.iedriver().setup();
            driver = new InternetExplorerDriver();
            break;

          default:
            logger.info("Browser not defined.");
            break;
        }
        break;

      case "grid":
        switch (browserName) {
          case "chrome":
            logger.info("Starting tests on chrome browser.");
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--start-maximized");
            chromeOptions.addArguments("--disable-extensions");
            chromeOptions.setCapability("platform", "LINUX");
            chromeOptions.addArguments("--start-maximized");
            driver = new RemoteWebDriver(new URL(prop.getProperty("GridServer")), chromeOptions);
            logger.info("Setting Up Selenium Grid.");
            break;

          case "firefox":
            logger.info("Starting tests on firefox browser.");
            FirefoxOptions ffOptions = new FirefoxOptions();
            ffOptions.addArguments("--start-maximized");
            ffOptions.setCapability("platform", "LINUX");
            driver = new RemoteWebDriver(new URL(prop.getProperty("GridServer")), ffOptions);
            logger.info("Setting Up Selenium Grid.");
            break;

          default:
            logger.info("Browser not defined.");
            break;
        }
        break;

      case "browser_stack":
        String username = prop.getProperty("bs_username");
        String automate_key = prop.getProperty("bs_key");
        String url = "http://" + username + ":" + automate_key + "@hub.browserstack.com:80/wd/hub";
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", prop.getProperty("browser"));
        caps.setCapability("browser_version", prop.getProperty("browser_version"));
        caps.setCapability("os", prop.getProperty("os"));
        caps.setCapability("os_version", prop.getProperty("os_version"));
        caps.setCapability("resolution", prop.getProperty("resolution"));
        caps.setCapability("project", prop.getProperty("project"));
        caps.setCapability("name", prop.getProperty("name"));
        caps.setCapability("browserstack.debug", prop.getProperty("browserstack.debug"));
        caps.setCapability(
            "browserstack.networkLogs", prop.getProperty("browserstack.networkLogs"));

        driver = new RemoteWebDriver(new URL(url), caps);

        logger.info("Setting Up Browser Stack Grid.");
        break;

      default:
        logger.info("No environment defined.");
        break;
    }

    
    driver
        .manage()
        .timeouts()
        .pageLoadTimeout(Long.parseLong(prop.getProperty("Timeout")), TimeUnit.SECONDS);
    driver.get(prop.getProperty("Url"));
    driver.manage().window().maximize();
    driver.manage().window().setSize(new Dimension(1200, 900));
    logger.info("Driver initialization completed.");
  }
}
