package com.block8.uiengine;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.block8.framework.reporting.ExtentManager;

import org.apache.commons.io.FileUtils;
import org.testng.TestNG;

public class UIExecute {
    public static String strRootPath;
    public static String strRepositories;
    public static String strRootDataFiles;

    String uisuites = "/uia/test-suites/";

    public UIExecute(String strRootPath){
        UIExecute.strRootPath = strRootPath;
        UIExecute.strRepositories = "F://mystake//Automation//APIAutomation//TestAutomation//BAAF_Root//uia//repositories";
        //UIExecute.strRepositories = UIExecute.strRootPath + "/uia/repositories/";
        UIExecute.strRootDataFiles ="F://mystake//Automation//APIAutomation//TestAutomation//BAAF_Root//uia//data";
        //UIExecute.strRootDataFiles = UIExecute.strRootPath + "/uia/data/";
    }

    public void startUITest(){
        TestNG testRunner = new TestNG();
        List<String> suitefiles = new ArrayList<String>();
        Iterator<File> listFiles = FileUtils.iterateFiles(new File(UIExecute.strRootPath + uisuites),
                new String[] { "xml" }, false);
        while(listFiles.hasNext()){
            File file = listFiles.next();
            suitefiles.add(file.getAbsolutePath());
        }
        testRunner.setTestSuites(suitefiles);
        testRunner.run();
    }
}