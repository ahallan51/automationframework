package com.selenium.testcases;



import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class LoginTests extends ConfigTests {
	
	WebDriver driver ;
  public LoginTests() throws Exception {

    /**
     * ** Provide path to excel, Name of excel sheet and name of the repositories TestData.xlsx -
     * Name of excel containing data PlayChipData - Name of excel sheet containing data
     * PlayChipLogin.txt - Name of the file containing locators
     */
    super("TestData.xlsx", "PlayChipData", "PlayChipCentralRepo.txt");
    testCaseName = this.getClass().getName();
    
  }

   
  @Test(priority = 1)
  public void verifyLoginFunctionality() {

    try {
    	if(!login.loginToApplication(getData("UserName"),getData("Password"))){
    		_softAssert.fail("Unable to login to application.");	
    	}
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    _softAssert.assertAll();
  }

  // //@Test(priority = 1)
  // public void testingError() {
  //   for (int i = 0; i < 100; i++) {
  //     verifyLoginFunctionality();
  //   }
  // }
  /*@AfterMethod
  public void tearDown(){
      if (driver != null) {
      driver.manage().deleteAllCookies();
      driver.quit();
      }
  }*/
	
//if (!login.loginToApplication(
//     getData("UserName"), getData("Password"), getData("SecretKey").trim())) {
//  _softAssert.fail("Unable to login to application.");
//}
//
// if (!login.logOutFromApplication()) {
//   _softAssert.fail("Unable to loggedout from application.");
// }
}
