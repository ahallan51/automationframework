package org.baaf.execution;

import java.util.HashMap;
import java.util.Set;
import org.baaf.api.APIHeaders;
import org.baaf.api.APIMethods;
import org.baaf.api.APIResponse;
import org.baaf.api.requests.APIRequest;
import org.baaf.api.requests.GETRequest;
import org.baaf.api.requests.PATCHRequest;
import org.baaf.api.requests.PUTRequest;
import org.baaf.api.requests.PostRequest;
import org.baaf.api.sharables.Shareable;
import org.baaf.api.sharables.SharedValues;
import org.baaf.api.validators.ResponseValidator;
import org.baaf.utilities.json.JsonHelper;

public class CreateAPI {
    
    APIMethods amMethod;
    String strAPIName;
    String strURString;
    String srBaseURL;
    APIHeaders aHeaders;
    JsonHelper jsAPIDetails;
    APIRequest iapiRequest;
    HashMap<String,String> erDataStuffer;
    APIResponse arAPIResponsse;

    public CreateAPI(String strTemplatePath, HashMap<String,String> erDataFeeder) {
        
        this.jsAPIDetails = JsonHelper.parseJsonFile(strTemplatePath);
        this.erDataStuffer = erDataFeeder;
    }

    public void loadAPI(String strBaseUrl, Shareable shVariables){
        this.srBaseURL = strBaseUrl;
        //System.out.println("str" +strBaseUrl);
        amMethod = getAPIMethod(jsAPIDetails.searchValue("Method"));
        //System.out.println("am" +amMethod);
        switch(amMethod){
            case GET:
                iapiRequest = new GETRequest(amMethod,jsAPIDetails,shVariables);
                break;
            case POST:
                iapiRequest = new PostRequest(amMethod,jsAPIDetails,shVariables);
                break;
            case PUT:
                iapiRequest = new PUTRequest(amMethod,jsAPIDetails,shVariables);
                break;
            case PATCH:
                iapiRequest = new PATCHRequest(amMethod,jsAPIDetails,shVariables);
                break;
            default:
                System.out.println("API type not defined yet");
        }
        iapiRequest.setBaseURL(strBaseUrl);
      // System.out.println(strBaseUrl);
        iapiRequest.setURI(jsAPIDetails.searchValue("path"));
        iapiRequest.setAPIMethod(amMethod);
    }

    public void executeAPI(){
        try{
            System.out.println(iapiRequest.getAPIURI());
            iapiRequest.prepareAPI(aHeaders,erDataStuffer);
           // System.out.println("aheader"+aHeaders);
            //System.out.println(" Executing test data iterations:" + erDataStuffer.get("SrNo"));
           //System.out.println("URI : " + iapiRequest.getAPIURI());
            arAPIResponsse = iapiRequest.execute();
           System.out.println("arp" +arAPIResponsse.toString()); 
            APIEngine.logger.info("API Method: " + amMethod);
        }catch(Exception e){
//            System.out.println("Status codes ::" + arAPIResponsse.getHTTPSATUSCODE());
            APIEngine.logger.error(e.getClass() + "executeAPI Execution Failure: " + e.getMessage());
            APIEngine.logger.error(e.getClass() + "executeAPI Execution Failure: " + e.getStackTrace());
            e.printStackTrace();
        }
    }

    public APIResponse getAPIResponse(){
        try{
            System.out.println(iapiRequest.getAPIURI());
            iapiRequest.prepareAPI(aHeaders,erDataStuffer);
            System.out.println(" Executing test data iterations:" + erDataStuffer.get("SrNo"));
            System.out.println("URI : " + iapiRequest.getAPIURI());
            arAPIResponsse = iapiRequest.execute();
            APIEngine.logger.info("HTTP Code: " + arAPIResponsse.getHTTPSATUSCODE());
            APIEngine.logger.info("Respose Method: " + arAPIResponsse.toString());
        }catch(Exception e){
            APIEngine.logger.error( e.getClass() + "getAPIResponse Execution Failure: " + e.getClass() + e.getMessage());
            APIEngine.logger.error( e.getClass() + "getAPIResponse Execution Failure: " + e.getStackTrace().toString());
            e.printStackTrace();
        }
        return arAPIResponsse; 
}

    public APIMethods getAPIMethod(String strAPIMetod){
        APIMethods amMethodName;

        switch(strAPIMetod.toUpperCase()){
            case "GET":
                amMethodName = APIMethods.GET;
                break;
            case "POST":
                amMethodName = APIMethods.POST;
                break;
            case "PUT":
                amMethodName = APIMethods.PUT;
                break;
            case "PATCH":
                amMethodName = APIMethods.PATCH;
                break;
            default:
                amMethodName = APIMethods.UNDEFINED;
        }
        return amMethodName;
    }
    

    public ValidationResult validateResponse(){
        ValidationResult vResult = new ValidationResult();
        try{
            ResponseValidator rvValidator = new ResponseValidator(jsAPIDetails.readComplexValue("Validations"),arAPIResponsse);
            rvValidator.setExpectedData(erDataStuffer);
            rvValidator.validateHTTPCode(jsAPIDetails.searchValue("ExpectedStatus"));
            rvValidator.validateResponses();
            vResult.setFinalResult(true);
        }catch(Exception e){
            APIEngine.logger.error("validate Response Execution Failure: " + e.getMessage());
            APIEngine.logger.error("validate Response Execution Failure: " + e.getStackTrace());
            vResult.setFinalResult(false);
            vResult.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return vResult;
    }
    
    public boolean extractSharableValues(SharedValues svCommons) {
    	HashMap<String, String> valuesToSave = jsAPIDetails.readComplexValue("ExtractValues");
    	Set<String> listOfValues = valuesToSave.keySet();
    	try {
    	for (String string : listOfValues) {
    		System.out.println("Extracting shared Variable:: " + string);
    		String valueObtained = arAPIResponsse.readValue(valuesToSave.get(string));
			svCommons.set(string, valueObtained);
			System.out.println("Extracting values:: " + valueObtained);
		}
    	}catch(Exception eException) {
    		APIEngine.logger.error("Error saving shared values", eException);
    		return false;
    	}
    	System.out.println("Shared Values:: " + svCommons.toString());
    	return true;
    }

    public APIResponse getResponse(){
        return arAPIResponsse;
    }
}