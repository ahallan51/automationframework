package org.baaf.api.requests;


import static io.restassured.config.EncoderConfig.encoderConfig;

import java.util.HashMap;

import org.baaf.api.APIHeaders;
import org.baaf.api.APIMethods;
import org.baaf.api.APIResponse;
import org.baaf.api.sharables.Shareable;
import org.baaf.utilities.json.JsonHelper;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class PATCHRequest extends APIRequest {
	String strPayLoad;
    RequestSpecification req;
    
    public PATCHRequest(APIMethods amMethod, JsonHelper jsAPIDetails, Shareable shVariables) {
    	
    
        super(amMethod, jsAPIDetails,shVariables);
        req=given();
    }

    @Override
    public APIResponse execute() {
        APIResponse arAPIResponse = new APIResponse(amMethod);
        arAPIResponse.setResponse(req.patch(strAPIPath));
        return arAPIResponse;
    }

    @Override
    public void prepareAPI(APIHeaders ahAPIHeader, HashMap<String, String> erDataSet) {
        strAPIPath = updateURIParams(strAPIPath, erDataSet);
        req.headers(createHeaders(erDataSet).getHeaderMap());
        req.contentType("application/json");
        strPayLoad = createRequestPayLoad(erDataSet);
        System.out.println("Payload \n --" + strPayLoad );
        //req.config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("application/x-amz-json-1.1", ContentType.TEXT)));
        req.body(strPayLoad);
    }
    
}
