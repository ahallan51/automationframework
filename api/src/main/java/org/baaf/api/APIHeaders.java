package org.baaf.api;

import java.util.HashMap;
import java.util.Set;

public class APIHeaders {
    
    private HashMap<String, String> hmAPIHeaders;

    public APIHeaders(HashMap<String, String> hmAPIHeaders){
        this.hmAPIHeaders = hmAPIHeaders;
    }

    public APIHeaders(){
        this.hmAPIHeaders = new HashMap<>();
    }

    public boolean addHeaderValue(String strHeaderName, String strHeaderValue){
        hmAPIHeaders.put(strHeaderName, strHeaderValue);
        return true;
    }

    public String getHeaderValue(String strHeaderName){
        return hmAPIHeaders.get(strHeaderName);
    }

	public HashMap<String, String> getHeaderMap() {
		return hmAPIHeaders;
    }
    
    public Set<String> getHeaderList(){
        return hmAPIHeaders.keySet();
    }
}