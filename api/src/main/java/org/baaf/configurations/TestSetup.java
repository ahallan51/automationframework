package org.baaf.configurations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.block8.framework.data.ExcelReader;
import com.block8.framework.reporting.ExtentManager;
import org.apache.log4j.LogManager;
import org.baaf.restassured.RAHelper;
import org.baaf.utilities.json.JsonHelper;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.lang.reflect.Method;

public class TestSetup {
  public ExtentManager ex;
  ExtentTest extLogger;
  ExtentReports extent;
  String testCaseName;
  public static String APIrootFolder = "";
  ExcelReader eReader;
  protected String URIString;
  JsonHelper APIDetails;
  String strBaseURL;
  protected String[] listOfAPIs;
  protected String APITMethod = "testAPI";
  org.apache.log4j.Logger logger;

  public TestSetup() {
    extent = null;
    ex = new ExtentManager(extent, APIrootFolder);
    try {
      extent = ExtentManager.getReporter();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  @BeforeSuite
  public void setupTestSuite() {
    final Configurations conf = new Configurations("/test.properties");
    final HashMap<String, String> hm = conf.hmAPIDetails;
    APIrootFolder = (String) hm.get("RootPath");
    strBaseURL = hm.get("BASEURL");
    ArrayList<String> apiList = new ArrayList<>();
    try {
      eReader = ExcelReader.getInstance(APIrootFolder + "/Configurations.xlsx", "APIList");
      while(eReader.hasNext()){
        apiList.add(eReader.getData("API_Name"));
        System.out.println(eReader.getData("API_Name"));
        eReader.readNextRow();
      }
      listOfAPIs = new String[apiList.size()];
      apiList.toArray(listOfAPIs);
      apiList=null;
    } catch (final Exception e) {
      e.printStackTrace();
    }
    }

    @BeforeMethod
    public void setUp(final Method method) throws IOException {
      initiateTestCase(APITMethod);
      logger = LogManager.getLogger(this.getClass().getName());
      logger.info("################### Test Case Started: " + testCaseName + " ###################");
    }

    public void initiateTestCase(final String strAPIName){
      try {

        APIDetails = JsonHelper.parseJsonFile(TestSetup.APIrootFolder +"/requesttemplates/" + strAPIName +".json");
        testCaseName = APIDetails.searchValue("name");
        URIString = strBaseURL + APIDetails.searchValue("path");
        ex = new ExtentManager(extent, APIrootFolder);  
        extent = ExtentManager.getReporter();
      } catch (final IOException e) {
        e.printStackTrace();
      }
      extLogger = ExtentManager.getLogger(testCaseName);
    }

    @AfterMethod
    public void getResult(final ITestResult result) throws Exception {
    System.out.println("Test Reporter working!!! ");
      final List<String> messages = Reporter.getOutput();
      for (final String string : messages) {
        extLogger.log(Status.PASS, string);
      }
      if (result.getStatus() == ITestResult.FAILURE) {
        extLogger.log(
            Status.FAIL,
            MarkupHelper.createLabel(
                result.getMethod().getMethodName()+ " Test case FAILED due to below issues:", ExtentColor.RED));
        extLogger.fail((result.getThrowable()).getMessage());
  
      } else if (result.getStatus() == ITestResult.SKIP) {
        extLogger.log(
            Status.SKIP,
            MarkupHelper.createLabel(
                result.getName() + " Test case SKIPPED due to below issues:", ExtentColor.GREY));
        extLogger.skip((result.getThrowable()).getMessage());
  
      } else if (result.getStatus() == ITestResult.SUCCESS) {
        extLogger.log(
            Status.PASS,
            MarkupHelper.createLabel(result.getName() + " Test case PASSED.", ExtentColor.GREEN));
      }
      Reporter.clear();
    }

    public void executeTests(){
      for (String string : listOfAPIs) {
        initiateTestCase(string);
        RAHelper ra= new RAHelper(null);
        ra.execute(string);
      }
    }
    
    @AfterSuite
    public void driverTearDown() {
      extent.flush();
    }
}