package org.baaf.manager.test;

import com.block8.framework.data.ExcelReader;

import org.baaf.execution.CreateAPI;
import org.baaf.execution.LoadConfigurations;
import org.testng.annotations.Test;

@Test
public class CreateAPITest {
    LoadConfigurations lcConf;

    @Test
    public void testCreateHeaders() {
        lcConf = new LoadConfigurations();
        String jsHelp = lcConf.getRootFolder() + "/requesttemplates/testAPI.json";
        ExcelReader readData = ExcelReader.getInstance(lcConf.getRootFolder()+"/TestData/testAPI.xlsx", "testData");
        CreateAPI crtRequest = new CreateAPI(jsHelp,readData.getFullDataset());
        crtRequest.executeAPI();
    }
}