*************** Common*****************
PlayChip.Page.Title=PlayWallet

*************** login*****************
Login.loginbtn=login-btn
Login.Username=xpath$|$//input[@name='email']
Login.Password=xpath$|$//input[@name="password"]
Login.Submit=xpath$|$////button[@type="submit"]

*************** Dashboard *****************
Dashboard.Logout=xpath$|$//button[text()='Logout']
Page.Menu.Dashboard=xpath$|$//a[@href='/dashboard']
Dashboard.Balance.Label.Heading=xpath$|$//h6[contains(text(),'Balances')]

Dashboard.Balance.Label.Free=xpath$|$//h6[contains(text(),'PlayChip Free')]
Dashboard.Balance.Label.Paid=xpath$|$//h6[contains(text(),'PlayChip Paid')]

Dashboard.Balance.Value.Free=xpath$|$//div[.='PlayChip Free']/./following-sibling::div//span
Dashboard.Balance.Value.Paid=xpath$|$//div[.='PlayChip Paid']/./following-sibling::div//span

Dashboard.Balance.Link.ConvertBalance=linktext$|$Convert Balances
Dashboard.Transactions.Label.Heading=xpath$|$//h6[contains(text(),'Recent Transactions')]
Dashboard.TopMenu.Link.Logout=xpath$|$//div[.='Logout' and @class='buttons']
Dashboard.TopMenu.Link.Play=xpath$|$//a[contains(text(),'Play')]
Dashboard.TopMenu.Link.Settings=xpath$|$//a[.='Settings']
Dashboard.TopMenu.Link.Support=xpath$|$//a[.='Support']

*************** Support *************************
Support.Windows.Name=Support : PlayWallet Support
Support.TopMenu=xpath$|$//div[@id='header-tabs']/a
Support.TopMenu.Text=Home,Solutions

*************** Play *************************
Play.Div.PlayNow=xpath$|$//div[contains(@class,'ezeteh')]//div[.='Play Now']/parent::div
Play.Div.PlayNow.Links=xpath$|$//div[contains(@class,'ezeteh')]//div[.='Play Now']/parent::div//a
Play.Link.PlayNow=xpath$|$//div[contains(@class,'ezeteh')]//div[.='Play Now']
PlayXchange.WindowName.NewPlayWindow=PlayWallet
PlayXchange.Text.Message=//div[@class='css-4b375e']
PayXchange.WindowTitles.Links=Partner Website

*************** Transactions *****************
Page.Menu.Transactions=xpath$|$//a[@href='/transactions']
Transactions.Partener.Selection=xpath$|$//select[@id='partner']
Transactions.Pagination.NextButton=xpath$|$//button[contains(@class,'pagination-next')]
Transactions.Selection.List=xpath$|$//select[@id='partner']/option[text()='localhost']
Transactions.Selection.resultMessage=xpath$|$//p[contains(@class,'css')][1]

*************** WithDraw *****************
Page.Menu.Withdraw=xpath$|$//a[contains(@href,'/withdraw')]
Page.Withdraw.SendToInput=xpath$|$//input[@id='address']
Page.Withdraw.AmountInput=xpath$|$//input[@id='amount']
Page.Withdraw.SubmitButton=xpath$|$//button[@type='submit']
Page.Withdraw.SuccessOkButton=xpath$|$//button[@class='swal2-confirm swal2-styled']
Page.Withdraw.ConfirmButton=xpath$|$//button[contains(@class,'is-success')]
Page.Withdraw.ConfirmSuccessMsg=xpath$|$//h2[@class='swal2-title']
EmailContent.LinkIndex.String=withdrawal process
AWS.S3.Bucket.Name=playchip-test-mailbox

*************** Settings *****************
Settings.Text.Message=xpath$|$//h1[contains(@class,'danger')]
Settings.Text.Heading=xpath$|$//h1[contains(@class,'title'][1]
Settings.Labels.FirstName=xpath$|$//label[contains(text(),'First Name')]
Settings.Labels.MiddleName=xpath$|$//label[contains(text(),'Middle Name')]
Settings.Labels.FirstName=xpath$|$//label[contains(text(),'Last Name')]
Settings.Labels.Email=xpath$|$//label[contains(text(),'Email')]
Settings.Labels.DoB=xpath$|$//label[contains(text(),'Date of Birth')]
Settings.Labels.Address=xpath$|$//label[contains(text(),'Date of Birth')]
Settings.Labels.StreetAddress=xpath$|$//label[contains(text(),'Street Address')]
Settings.Labels.CityTown=xpath$|$//label[contains(text(),'City / Suburb / Town')]
Settings.Labels.Country=xpath$|$//label[contains(text(),'Country')]
Settings.Labels.State=xpath$|$//label[contains(text(),'State')]
Settins.Labels.PostCode=xpath$|$//label[contains(text(),'Post / Zip Code')]

Settings.Input.FirstName=xpath$|$//Input[@name='firstName']
Settings.Input.MiddleName=xpath$|$//Input[@name='middleName']
Settings.Input.LastName=xpath$|$//Input[@name='lastName']
Settings.Input.Email=name$|$email
Settings.Input.DoB=name$|$DoB
Settings.Input.StreetAddress=id$|$streetAddress
Settings.Input.City=city
Settings.Input.PostCode=name$|$postCode
Settings.Input.Coutry=name$|$countryId
Settings.Input.State=name$|$jurisdictionId
Settings.Iput.SaveChanges=xpath$|$//button[@type='submit']

Settings.VerifyIdentity.Complete.Message=xpath$|$//p[contains(text(),'Your identity has already been verified')]
Settings.VerifyIdentity.Link.VerifyIdentity=link$|$Verify Identity

Settings.LinkedPartners.Link.LinkedPartners=link$|$Linked Partners
Settings.LinkedPartners.Text.Heading=xpath$|$//div[contains(@class,'ezeteh')]//div[.='Linked Partners']

*************** Top Up *****************
Page.Menu.TopUp=xpath$|$//a[@href='/top-up']
Topup.Credit.Input.Amount=xpath$|$//input[contains(@id,'amount')]
Topup.Credit.Input.CardNumber=xpath$|$//input[@id='cardNumber']
Topup.Credit.Input.Name=xpath$|$//input[@id='name']
Topup.Credit.Input.ExpiryMonth=xpath$|$//select[@id='expiryMonth']
Topup.Credit.Input.ExpiryMonthList=xpath$|$//select[@id='expiryMonth']/option[text()='11']
Topup.Credit.Input.ExpireYear=xpath$|$//select[@id='expiryYear']
Topup.Credit.Input.CCV=xpath$|$//input[@id='cvn']
Topup.Credit.Input.ConfirmPaymentButton=xpath$|$//button[@type='submit']
Topup.Credit.Input.ModalCloseButton=xpath$|$//button[contains(@class,'close')]
Topup.Credit.Input.ErrorMessage=xpath$|$//div[contains(@class,'message')]/p

Topup.Crypto.Option=xpath$|$//a[contains(@href,'crypto')]
Topup.Credit.Option=xpath$|$//a[contains(@href,'fiat')]
Topup.RcvPlayChips.Option=xpath$|$//a[contains(@href,'receive')]

