package org.baaf.utilities.json;



import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;


public class JsonMessage {
	private final String EMAIL_CONTENT_JSONPATH= "$.body.data";
	
	String strJsonMessageRAW;
	Object document;
	
	public JsonMessage(String strJsonMesageRAW) {
		this.strJsonMessageRAW = strJsonMesageRAW;
		document = Configuration.defaultConfiguration().jsonProvider().parse(strJsonMesageRAW);
	}
	
	public String getSnippet() {
		return JsonPath.read(document, EMAIL_CONTENT_JSONPATH);
	}
	
	public String getJsonByPath(String strJSONPath) {
		return JsonPath.read(document, strJSONPath);
	}
}

