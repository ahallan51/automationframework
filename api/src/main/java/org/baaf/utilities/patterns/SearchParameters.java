package org.baaf.utilities.patterns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.baaf.api.sharables.Shareable;

public class SearchParameters {
    
    PatternMatcher pmSearchParameters;
    String strPatternMatching;

    public SearchParameters(String strPatternMatching){
        if(strPatternMatching==null){
            strPatternMatching = "\\$\\{.[^\\{}]*\\}";
        }
        this.strPatternMatching = strPatternMatching;
        pmSearchParameters = new PatternMatcher(this.strPatternMatching, 2, 1);
    }

    public ArrayList<String> getParameterList(String strString){
        ArrayList<String> alParameters = new ArrayList<>();
        pmSearchParameters.setFilterString(strString);
        while(pmSearchParameters.hasMoreMatches()){
            alParameters.add(pmSearchParameters.getStringFromPattern());
        }
        return alParameters;
    }

    public String updatedParameters(String strUpdateAble,HashMap<String, String> hmValuesMap){
        pmSearchParameters.setFilterString(strUpdateAble);
        pmSearchParameters.getMatchingIndexes();
        HashMap<String,String> paramsMap = pmSearchParameters.getPatterns();
        ArrayList<String> alParameterList = getParameterList(strUpdateAble);
        for (String string : alParameterList) {
            strUpdateAble = strUpdateAble.replace(paramsMap.get(string), hmValuesMap.get(string));
        }
        System.out.println("strupdatetable" +strUpdateAble);
        return strUpdateAble;
        
    }
    
    public String updatedVariables(String strUpdateAble,Shareable hmValuesMap){
        pmSearchParameters.setFilterString(strUpdateAble);
        pmSearchParameters.getMatchingIndexes();
        HashMap<String,String> paramsMap = pmSearchParameters.getPatterns();
        ArrayList<String> alParameterList = getParameterList(strUpdateAble);
        for (String string : alParameterList) {
            strUpdateAble = strUpdateAble.replace(paramsMap.get(string), hmValuesMap.get(string));
        }
        return strUpdateAble;
    }

    public HashMap<String,String> updateParameters(HashMap<String,String> hmUpdateAble,HashMap<String, String> hmValuesMap){
        Set<String> valuesToUpdate = hmUpdateAble.keySet();
        
        for (String string : valuesToUpdate) {
            String valueToUpdate = hmUpdateAble.get(string);
            pmSearchParameters.setFilterString(valueToUpdate);
            hmUpdateAble.put(string,updatedParameters(valueToUpdate, hmValuesMap));
        }
    return hmUpdateAble;
    }
    
    public HashMap<String,String> updateVariables(HashMap<String,String> hmUpdateAble,Shareable svShareable){
        Set<String> valuesToUpdate = hmUpdateAble.keySet();
        
        for (String string : valuesToUpdate) {
            String valueToUpdate = hmUpdateAble.get(string);
            pmSearchParameters.setFilterString(valueToUpdate);
            hmUpdateAble.put(string,updatedVariables(valueToUpdate, svShareable));
        }
    return hmUpdateAble;
    }
}