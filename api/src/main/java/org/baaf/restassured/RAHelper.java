package org.baaf.restassured;

import static io.restassured.RestAssured.*;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import org.baaf.api.APIHeaders;

public class RAHelper {

    APIHeaders ahApiHeaders;

    public RAHelper(APIHeaders ahApiHeaders) {
        this.ahApiHeaders = ahApiHeaders;
    }

    public void execute(String strAPI) {
        String response = given().get(strAPI).asString().toString();
        DocumentContext json = JsonPath.parse(response);
        System.out.println(json.jsonString());
    }

    public String validate(String strAPI) {
        String response = given().get(strAPI).asString().toString();
        DocumentContext json = JsonPath.parse(response);
        System.out.println(json.jsonString());
        return "Error";
    }
}