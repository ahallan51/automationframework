package org.baaf.utilities.test;

import org.baaf.configurations.TestSetup;
import org.baaf.utilities.json.JsonHelper;
import org.testng.annotations.Test;

@Test
public class JSONHelperTest extends TestSetup{
    
    @Test
    public void loadJson(){
        JsonHelper js = JsonHelper.parseJsonFile(TestSetup.APIrootFolder +  "/requesttemplates/testAPI.json");
        System.out.println(js.toString());
        System.out.println("=========================");
        System.out.println(js.readComplexObjectAsString("Payload"));
    }
}