package org.baaf.utilities.test;

import java.io.File;
import org.baaf.configurations.TestSetup;
import org.baaf.utilities.json.LoadfromFile;
import org.testng.annotations.Test;

@Test
public class LoadfromFileTest extends TestSetup{

    @Test
    public void testJSONLoad() {
        System.out.println("Starting test case");
        LoadfromFile loadfromFile = new LoadfromFile(
                new File(TestSetup.APIrootFolder + "/requesttemplates/testAPI.json"));
        loadfromFile.loadJSON();
        System.out.println(" Check JSON:: "+loadfromFile.getValue().jsonString());
    }
}