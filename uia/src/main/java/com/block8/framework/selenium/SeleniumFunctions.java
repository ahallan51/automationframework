package com.block8.framework.selenium;

//import java.util.List;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

public class SeleniumFunctions {

  private Boolean IsPass;
  private WebDriver driver = null;

  public SeleniumFunctions(String strReporsitoryPath, WebDriver driver) {
    this.driver = driver;
  }

  public void waitConditionfor(
      WebDriver driver, String byCondition, String byValue, String expected, int waitseconds) {
    int defaultTimeoutInSeconds = 60;

    if (waitseconds > defaultTimeoutInSeconds) defaultTimeoutInSeconds = waitseconds;

    switch (byCondition.toLowerCase()) {
      case "class":

        /*
         * WebDriverWait wait = new WebDriverWait(driver,
         * defaultTimeoutInSeconds);
         * wait.until(ExpectedConditions.visibilityOfElementLocated
         * (By.className(byValue)));
         */
        for (int second = 0; ; second++) {
          if (second >= defaultTimeoutInSeconds) {
            Reporter.log(
                "\n Required text: "
                    + expected
                    + " not present on page after "
                    + waitseconds
                    + "seconds");
            break;
          }
          try {
            if (isElementPresent(driver, By.className(byValue), expected)) break;
            Thread.sleep(1000);
          } catch (Exception e) {
          }
        }
        ;
        break;
      case "id":
        for (int second = 0; ; second++) {
          if (second >= waitseconds) {
            Reporter.log(
                "\n Required text: "
                    + expected
                    + " not present on page after "
                    + waitseconds
                    + "seconds");
            break;
          }
          try {
            if (isElementPresent(driver, By.id(byValue), expected)) break;
            Thread.sleep(1000);
          } catch (Exception e) {
          }
        }
        ;
        break;
      case "xpath":
        for (int second = 0; ; second++) {
          if (second >= waitseconds) {
            Reporter.log(
                "\n Required text: "
                    + expected
                    + " not present on page after "
                    + waitseconds
                    + "seconds");
            break;
          }
          try {
            if (isElementPresent(driver, By.xpath(byValue), expected)) break;
            Thread.sleep(1000);
          } catch (Exception e) {
          }
        }
        ;
        break;

      case "css":
        for (int second = 0; ; second++) {
          if (second >= waitseconds) {
            Reporter.log(
                "\n  Required text: "
                    + expected
                    + " not present on page after "
                    + waitseconds
                    + "seconds");
            break;
          }
          try {
            if (isElementPresent(driver, By.cssSelector(byValue), expected)) break;
            Thread.sleep(1000);
          } catch (Exception e) {
          }
        }
        ;
        break;
      case "tag":
        for (int second = 0; ; second++) {
          if (second >= waitseconds) {
            Reporter.log(
                "\n Required text: "
                    + expected
                    + " not present on page after "
                    + waitseconds
                    + "seconds");
            break;
          }
          try {
            if (isElementPresent(driver, By.tagName(byValue), expected)) break;
            Thread.sleep(1000);
          } catch (Exception e) {
          }
        }
        ;
        break;
      default:
        Reporter.log("\n " + byCondition + "option is not exist.Please check it again ");
        break;
    }
  }

  private Boolean isElementPresent(WebDriver driver, By by, String expected) {
    boolean result = false;
    try {
      if (expected != null) {
        String actual = driver.findElement(by).getText();
        if (actual == expected) result = true;
        // return true;
        // return false;
      } else {
        driver.findElement(by);
        result = true;
        // return true;
      }
    } catch (NoSuchElementException e) {
      // return false;
    }

    return result;
  }

  public void typeText(WebDriver driver, String elementIdentfier, String text) {
    this.driver = driver;
    WebElement element = null;
    int waitCount = 0;

    try {
      element = getElement(elementIdentfier, driver);
      if (element == null) {
        element = getElement(elementIdentfier, driver);
      }
    } catch (Exception e) {
    }

    while (waitCount != 2) {
      try {
        /*
         * if ((element.getAttribute("value")) != null ||
         * !(element.getAttribute("value")).isEmpty()) {
         * element.clear(); } if ((element.getText()) != null ||
         * !(element.getText()).isEmpty()) { element.clear(); }
         */
        if (((element.getText()) != null || !(element.getText()).isEmpty())
            || ((element.getAttribute("value")) != null
                || !(element.getAttribute("value")).isEmpty())) {
          element.clear();
        }
        if (text != null) {
          element.sendKeys(text);
        }
        Reporter.log("\n Successfully entered text " + text);
        IsPass = true;

      } catch (Exception e) {
        Reporter.log(
            "<span style=\"color:red\"><b>Error typing text. " + e.getMessage() + "</b></span>");
        try {
          Thread.sleep(5000);
        } catch (InterruptedException e1) {
          // TODO Auto-generated catch block
          Reporter.log(e1.getMessage());
        }
        waitCount++;
        IsPass = false;
      }

      if (IsPass) {
        break;
      }
    }
  }

  public void clickElement(WebDriver driver, String elementIdentfier) {
    this.driver = driver;
    WebElement element = null;
    //int waitCount = 0;

    try {
      element = getElement(elementIdentfier, driver);
      if (element != null) {
        element.click();
        Reporter.log("\n Successfully Clicked" + element.getText());
      }
    } catch (Exception e) {
      Reporter.log(
          "<span style=\"color:red\"><b>Error typing text. " + e.getMessage() + "</b></span>");
      //TestResult.FAILURE
    }
  }

  public WebElement getElement(String elementIdentifier, WebDriver driver) {
    WebElement element = null;
    try {
      element = getElements(elementIdentifier, driver);
    } catch (Exception e) {
      Reporter.log("\n Unable to find the element " + e.getMessage());
    }
    return element;
  }

  /*
   * public WebElement getElement(String elementIdentifier, WebElement
   * parentElement) { int index = 0; WebElement element = null;
   * Integer.parseInt(ObjectProperties.getProperty(elementIdentifier,
   * ObjectProperties.INDEX));
   * //int.TryParse(Props.getProperty(elementIdentifier, Props.INDEX), out
   * index); try { element = getElements(elementIdentifier,
   * parentElement)[index]; }
   *
   * catch (Exception e) {
   * Reporter.log("Unable to find the element "+e.getMessage() ); } return
   * element; }
   */
  public WebElement getElements(String identifier, WebDriver driver) {
    String type = null;
    String locator = null;
    int index = 0;
    WebElement element = null;
    Reporter.log("String: " + identifier);
    String stringSeparators = "\\$\\|\\$";
    String[] splitString = identifier.split(stringSeparators);

    switch (splitString.length) {
      case 1:
        // locator
        type = "id";
        locator = identifier;
        break;
      case 2:
        if (splitString[1] != null) {
          if (Pattern.matches(splitString[1], "^[0-9]")) {
            // locator,index
            type = "id";
            locator = splitString[0];
          } else {
            // type,locator
            type = splitString[0];
            locator = splitString[1];
          }
        }
        break;
      case 3:
        // type,locator,index
        type = splitString[0];
        locator = splitString[1];
        if (splitString[2] != null) {
          index = Integer.parseInt(splitString[2]);
        }

        break;

      default:
        Reporter.log("\n Length is not matching: type=" + splitString.length + " Please check it ");
        break;
    }

    // WebDriverWait wait = new WebDriverWait(driver,
    // TimeSpan.FromSeconds(60));
    WebDriverWait wait = new WebDriverWait(driver, 120);
    try {
      switch (type.toLowerCase()) {
        case "xpath":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
          element = driver.findElement(By.xpath(locator));
          break;
        case "id":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
          element = driver.findElement(By.id(locator));
          break;
        case "name":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
          element = driver.findElement(By.name(locator));
          break;
        case "link":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(locator)));
          element = driver.findElement(By.linkText(locator));
          break;
        case "partiallink":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(locator)));
          element = driver.findElement(By.partialLinkText(locator));
          break;
        case "class":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
          element = driver.findElement(By.className(locator));
          break;

        case "tag":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(locator)));
          element = driver.findElement(By.tagName(locator));
          break;
        case "css":
          wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
          element = driver.findElement(By.cssSelector(locator));
          break;
        default:
          Reporter.log(
              "\n Unable to find element: type=" + type + " locator=" + locator + "\nException: ");
          break;
      }
      return element;
    } catch (Exception e) {

      Reporter.log(
          "\n Unable to find element: type="
              + type
              + " locator="
              + locator
              + "\nException: "
              + e.getMessage());
      return element;
    }
  }

  public void openURL(WebDriver driver, String url) {
    try {
      driver.navigate().to(url.trim());
      Reporter.log("\n Successfully navigated to URL: " + url);
    } catch (Exception e) {
      Reporter.log("\n Unable to navigate to URL: " + url + ". Exception = " + e.getMessage());
      Assert.fail(e.getMessage());
    }
  }
}
