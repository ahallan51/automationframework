package org.baaf.api;

import java.util.HashMap;
import java.util.Set;

import org.baaf.utilities.json.JsonHelper;

public class PayLoadJson {
    JsonHelper json;
    String strPayLoadString;
    
    public PayLoadJson(String strPayLoadString){
        this.strPayLoadString = strPayLoadString;
    }

    public boolean validateResponse(String key, String expectedValue) {
        return json.searchValue(key).equalsIgnoreCase(expectedValue);
    }

    public JsonHelper getUpdatedPayLoad(HashMap<String,String> hmDataSet){
        
        Set<String> keyList = hmDataSet.keySet();
        for (String strValue : keyList) {
            if(strPayLoadString.contains("#{" +strValue+"}")){
                strPayLoadString = strPayLoadString.replace("#{" +strValue+"}", hmDataSet.get(strValue));
            }
        }
        return JsonHelper.parseJsonString(strPayLoadString);
    }

}