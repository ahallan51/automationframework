package org.baaf.api.validators.test;

import com.block8.framework.data.ExcelReader;

import org.baaf.execution.CreateAPI;
import org.baaf.execution.LoadConfigurations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@Test
public class ResponseValidatorTest {

    public static boolean debug;
	LoadConfigurations lcConfigurations;
    String strRootFolder;
    ExcelReader erAPISuite, erDataFeeder;
    String strCurrentAPIName;
    String strTestDataFile;
    String strDataSheet;
    CreateAPI capiAPIRequest;
    
    @BeforeTest
    public void setupAPI(){
        this.lcConfigurations = new LoadConfigurations();
        this.strRootFolder = lcConfigurations.getRootFolder();
        erAPISuite = ExcelReader.getInstance(strRootFolder + "/Configurations.xlsx", "APIList");
    }

    @Test
    public void validateHTTPCode(){
        String strAPIName = erAPISuite.getData("API_Name");
        String strJSONFile = strRootFolder + "/requesttemplates/" + strAPIName + ".json";
        strDataSheet = erAPISuite.getData("DataSheet");
        strTestDataFile = strRootFolder + "/TestData/" + erAPISuite.getData("DataFiles") + ".xlsx";
        erDataFeeder = ExcelReader.getInstance(strTestDataFile, strDataSheet);
        capiAPIRequest = new CreateAPI(strJSONFile, erDataFeeder.getFullDataset());
        capiAPIRequest.loadAPI(erAPISuite.getData("URL"),null);
        capiAPIRequest.executeAPI();
        capiAPIRequest.validateResponse();
        System.out.println(capiAPIRequest.getAPIResponse().toString());
        
    }
}