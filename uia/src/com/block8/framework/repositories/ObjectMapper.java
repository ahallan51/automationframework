package com.block8.framework.repositories;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.testng.Reporter;

import com.block8.framework.data.PropertiesReader;

public class ObjectMapper {

  String strRepositoryPath;
  Properties prObjectMapper;
  public static String TYPE = "type";
  public static String LOCATOR = "locator";
  public static String INDEX = "index";

  public ObjectMapper(String strRepositoryPath) throws IOException {
    this.strRepositoryPath = strRepositoryPath;
    this.prObjectMapper = PropertiesReader.getInstance(strRepositoryPath).getRepositoryObject();
  }

  public void updateRepositoryStore(String strFileName) throws IOException {
    this.strRepositoryPath = strFileName;
    this.prObjectMapper = PropertiesReader.getInstance(strFileName).getRepositoryObject();
  }

  public Set<Object> getElementNames() {
    return this.prObjectMapper.keySet();
  }

  public boolean isElementDefined(String strElementName) {
    return this.prObjectMapper.containsKey(strElementName);
  }

  public By getElementLocator(String strElementName) {
    String strIdentifier = (String) prObjectMapper.get(strElementName);
    String strLocatorType = null, strLocator = null;
    By byElement = null;
    int iELementIndex = -1;

    String stringSeparators = "\\$\\|\\$";
    String[] splitString = strIdentifier.split(stringSeparators);

    switch (splitString.length) {
      case 1:
        // locator
        strLocatorType = "id";
        strLocator = strIdentifier;
        break;
      case 2:
        if (splitString[1] != null) {
          if (Pattern.matches(splitString[1], "^[0-9]")) {
            // locator,index
            strLocatorType = "id";
            strLocator = splitString[0];
          } else {
            // type,locator
            strLocatorType = splitString[0];
            strLocator = splitString[1];
          }
        }
        break;
      case 3:
        // type,locator,index
        strLocatorType = splitString[0];
        strLocator = splitString[1];
        if (splitString[2] != null) {
          iELementIndex = Integer.parseInt(splitString[2]);
        }
        break;

      default:
        Reporter.log("\n Length is not matching: type=" + splitString.length + " Please check it ");
        break;
    }

    // WebwdDriverWait wait = new WebwdDriverWait(wdDriver,
    // TimeSpan.FromSeconds(60));
    //		WebDriverWait wait = new WebDriverWait(wdDriver, 120);
    try {
      switch (strLocatorType.toLowerCase()) {
        case "xpath":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.xpath(strLocator)));
          //				wbeElement = wdDriver.findElement(By.xpath(strLocator));
          if (iELementIndex > 0)
            byElement = By.xpath(strLocator + "[" + String.valueOf(iELementIndex) + "]");
          else byElement = By.xpath(strLocator);
          break;
        case "id":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.id(strLocator)));
          //				wbeElement = wdDriver.findElement(By.id(strLocator));
          byElement = By.id(strLocator);
          break;
        case "name":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.name(strLocator)));
          //				wbeElement = wdDriver.findElement(By.name(strLocator));
          byElement = By.name(strLocator);
          break;
        case "link":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.linkText(strLocator)));
          //				wbeElement = wdDriver.findElement(By.linkText(strLocator));
          byElement = By.linkText(strLocator);
          break;
        case "partiallink":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.partialLinkText(strLocator)));
          //				wbeElement = wdDriver.findElement(By.partialLinkText(strLocator));
          byElement = By.partialLinkText(strLocator);
          break;
        case "class":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.className(strLocator)));
          //				wbeElement = wdDriver.findElement(By.className(strLocator));
          byElement = By.partialLinkText(strLocator);
          break;

        case "tag":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.tagName(strLocator)));
          //				wbeElement = wdDriver.findElement(By.tagName(strLocator));
          byElement = By.tagName(strLocator);
          break;

        case "css":
          //				wait.until(ExpectedConditions.visibilityOfElementLocated(By
          //						.cssSelector(strLocator)));
          //				wbeElement = wdDriver.findElement(By.cssSelector(strLocator));
          byElement = By.cssSelector(strLocator);
          break;
        default:
          Reporter.log(
              "\n Unable to find element: type="
                  + strLocatorType
                  + " locator="
                  + strLocator
                  + "\nException: ");
          break;
      }
      return byElement;
    } catch (Exception e) {
      Reporter.log(
          "\n There was an error finding elements: type="
              + strLocatorType
              + " locator="
              + strLocator
              + "\nException: ");
    }
    return byElement;
  }

  public String getRawLocator(String strElementName) {
    return prObjectMapper.getProperty(strElementName);
  }
}
