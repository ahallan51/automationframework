package org.baaf.api.test;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import io.restassured.RestAssured;//static import of RA class. everthing will be imported from this class

import java.util.HashMap;
import java.util.Map;

//import io.restassured.response.Response;
import io.restassured.http.ContentType;

public class practice extends RestAssured {

    // @Test
    public void Get_01(String contentTypeString, String link, int statusCode, String firstname) {
        // baseURI = "http://dummy.restapiexample.com";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", contentTypeString);
        headers.put("Acess_Token", "cccctxseeazszhhgcghchg");
        headers.put("Jwt_token", "sdgdgcgvgvgtdreawawercgcgh");

        baseURI = "https://reqres.in";

        given().headers(headers).given().param("first_name", firstname).get(link).then().statusCode(statusCode).log()
                .all();
        System.out.println();
        // also using log()all() it will show the whole body in response.
        // If you don't use log it will not logg anything.It will just show the status
        // as passes.

    }

    @Test
    public void Post_01() {

        // While creating a post Req we need a body and for that i have used
        // JSON-LIBRARY like simple JSON.
        // used the request variable to create my request.
        JSONObject request = new JSONObject();
        request.put("name", "subina");
        request.put("job", "QA");
        request.put("id", "200");

        baseURI = "https://reqres.in";// Setting the path.

        // Here i am passing the type is of JSON-Type(Content-Type)
        // and the response is also accepted is of JSON-Type
        // in body i have used JSOn.String so that it converts into this format.
        given().contentType(ContentType.JSON).accept(ContentType.JSON).header("Content-Type", "application/Json")
                .body(request.toJSONString()).when().post("/api/users").then().statusCode(201).log().all();
    }

    // FOR PUT REQUEST:
    // public void Put_01() {

    // JSONObject request = new JSONObject();
    // request.put("name", "smile");//Just update the values which you want.
    // request.put("job", "dev");
    // request.put("id", "99");

    // baseURI = "https://reqres.in";
    // given().contentType(ContentType.JSON).accept(ContentType.JSON).header("Content-Type",
    // "application/Json")
    // .body(request.toJSONString()).when().put("/api/users").then().statusCode(200).log().all();

}

// FOR DELETE REQUEST
// public void Delete_01() {

// baseURI = "https://reqres.in";
// when().delete("/api/users")//endpoint or source to delete(/users/4-id which
// you want to del.)
// .then().statusCode(200)//status code 200 or 204 both can be fine
// .log().all();
