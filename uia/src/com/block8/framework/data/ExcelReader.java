package com.block8.framework.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/** * ExcelReader reads data from excel interactively */
public class ExcelReader implements DataFeeder {

  String strFilePath;
  XSSFWorkbook wbExcelFile;
  XSSFSheet xlshSheet;
  HashMap<String, String> hmCurrentDataSet, hmHeaders;
  int iCurrentRowNumber = 0, iLastRowNumber = Integer.MIN_VALUE;

  private ExcelReader(String strFilePath, String strSheetName) throws Exception {
    this.strFilePath = strFilePath;
    this.hmCurrentDataSet = new HashMap<String, String>();
    this.hmHeaders = new HashMap<String, String>();
    try {
      this.wbExcelFile = new XSSFWorkbook(new FileInputStream(new File(strFilePath)));
    } catch (IOException e) {
      throw new IOException("There was an error while opening workbook \n" + e.getMessage(), e);
    }
    setExcelSheet(strSheetName);
  }

  public void setExcelSheet(String sheetName) throws Exception {
    try {
      iCurrentRowNumber = 0;
      xlshSheet = wbExcelFile.getSheet(sheetName);
      this.iLastRowNumber = getRowCount(xlshSheet);
      readHeaders();
      readNextRow();
    } catch (Exception Ex) {
      throw new Exception("There was an error while changing Sheet - \n" + Ex.getMessage(), Ex);
    }
  }

  public int getRowCount(XSSFSheet excelWSheet) {
    return excelWSheet.getLastRowNum() + 1;
  }

  public int getColumnCount(XSSFSheet excelWSheet) {
    return excelWSheet.getRow(0).getLastCellNum() + 1;
  }

  public String getCellData(int RowNum, int ColNum) throws Exception {
    String cellData = null;
    try {
      XSSFCell cell = xlshSheet.getRow(RowNum).getCell(ColNum);
      DataFormatter formatter = new DataFormatter();
      cellData = formatter.formatCellValue(cell);
    } catch (Exception e) {
      throw new Exception("There was an erorr while reading cell data", e);
    }
    return cellData;
  }

  public String getCellData(XSSFCell cell) throws Exception {
    String cellData = null;
    try {
      DataFormatter formatter = new DataFormatter();
      cellData = formatter.formatCellValue(cell);
    } catch (Exception e) {
      throw new Exception("There was an erorr while reading cell data", e);
    }
    return cellData;
  }

  public boolean readHeaders() throws Exception {
    hmHeaders.clear();
    XSSFRow headerRow = getRowData(0, xlshSheet);
    int iCounter = 0;
    for (Cell cell : headerRow) {
      hmHeaders.put(getCellData((XSSFCell) cell), String.valueOf(iCounter));
      iCounter++;
    }
    return true;
  }

  public XSSFRow getRowData(int RowNum, XSSFSheet excelWSheet) {
    XSSFRow row = null;
    try {
      row = excelWSheet.getRow(RowNum);
    } catch (Exception Ex) {
      Ex.printStackTrace();
    }
    return row;
  }

  public boolean readNextRow() throws Exception {
    ++iCurrentRowNumber;
    if(!hasNext()){
      return false;
    }
    hmCurrentDataSet.clear();
    XSSFRow xrCurrentRow = getRowData(iCurrentRowNumber, xlshSheet);
    for (String key : hmHeaders.keySet()) {
      int cellNumber = Integer.valueOf(hmHeaders.get(key));
      hmCurrentDataSet.put(key, getCellData(xrCurrentRow.getCell(cellNumber)));
    }
    return true;
  }

  public boolean hasNext() {
    return iLastRowNumber > iCurrentRowNumber;
  }

  @Override
  public String isValid() {
    return null;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String getData(String strColumnName) {
    return hmCurrentDataSet.get(strColumnName);
  }

  public HashMap<String, String> getFullDataset() {
    return hmCurrentDataSet;
  }

  public static ExcelReader getInstance(String strFilePath, String strSheetName) {
    ExcelReader xlrReadData=null;
    try {
      xlrReadData = new ExcelReader(strFilePath, strSheetName);
    } catch (Exception e) {
      System.out.println("Error reading Excel file :" + strFilePath);
      e.printStackTrace();
    }
    return xlrReadData;
  }

  public boolean close() throws IOException {
    try {
      wbExcelFile.close();
    } catch (IOException e) {
      throw new IOException("There was an error during closing excel" + e.getMessage(), e);
    }
    return true;
  }
}
