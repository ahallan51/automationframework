package org.baaf.utilities.test;

import org.testng.annotations.Test;

import com.google.gson.JsonObject;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static io.restassured.config.EncoderConfig.encoderConfig;;


public class testAPI {
  @Test
  public void executeAPI() {
	 RequestSpecification request = given().config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("application/zip", ContentType.TEXT)));
		request.header("content-type","application/x-amz-json-1.1");
		request.header("Connection","keep-alive");
		request.header("Accept-Encoding","gzip, deflate, br");
		request.header("authority","cognito-idp.ap-southeast-2.amazonaws.com");
		request.header("cache-control","max-age=0");	
		request.header("x-amz-target","AWSCognitoIdentityProviderService.InitiateAuth");
		request.header("x-amz-user-agent","aws-amplify/0.1.x js");	
		request.header("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36");
		request.header("accept","*/*");
		request.header("origin","http://qualifier-apitest-env.s3-website-ap-southeast-2.amazonaws.com/");
		request.header("sec-fetch-site","cross-site");
		request.header("sec-fetch-mode","cors");
		request.header("sec-fetch-dest","empty");
		request.header("referer","http://qualifier-apitest-env.s3-website-ap-southeast-2.amazonaws.com/");
		request.header("accept-language","en-IN,en-US;q=0.9,en;q=0.8");
		
		JsonObject person1 = new JsonObject();
		person1.addProperty("AuthFlow", "USER_PASSWORD_AUTH");
		person1	.addProperty("ClientId", "6n6rfcs2ci5tgs38t84tib2nbq");
		// Create Inner JSON Object
		JsonObject address = new JsonObject();
		address.addProperty("USERNAME", "admin0@block8.io");
		address.addProperty("PASSWORD", "Password1!");
		
		person1.add("AuthParameters", address);
		
		person1	.add("ClientMetadata", new JsonObject());
		
		System.out.println(person1.toString());
		
		Response res = request.config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("application/x-amz-json-1.1", ContentType.TEXT))).body(person1).post("https://cognito-idp.ap-southeast-2.amazonaws.com/");
		System.out.println(res.prettyPrint());

  }
}
