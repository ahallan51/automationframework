package com.selenium.pages;

import java.util.Properties;
import com.block8.framework.repositories.Repository;
import com.block8.framework.selenium.ReusableFunctions;
import org.apache.log4j.Logger;
import org.jboss.aerogear.security.otp.Totp;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

public class LoginPage {
  private Logger logger;
  ReusableFunctions reuse;
  Repository rpObjects;

  private String loginUsername = "Login.Username";
  private String loginPassword = "Login.Password";
  private String loginSubmit = "Login.Submit";

  public LoginPage(WebDriver driver, Properties prop, Repository rpObjects) {
    super();
    reuse = new ReusableFunctions(driver, prop);
    logger = Logger.getLogger(LoginPage.class.getName());
    this.rpObjects = rpObjects;
  }

  public boolean loginToApplication(String Username, String Password) {
    boolean flag = false;
    try {
      logger.info("Logging into application.");
      Reporter.log("Enter User Name");
      reuse.enterText(rpObjects.getElement(loginUsername), Username);
      Reporter.log("Enter Password");
      reuse.enterText(rpObjects.getElement(loginPassword), Password);
      Reporter.log("Click Submit");
      reuse.click(rpObjects.getElement(loginSubmit));
    } catch (Exception Ex) {
      logger.error("Exception occurred while logging into application: " + Ex.getMessage());
    }
    return flag;
  }

//  public boolean loginToApplication(String Username, String Password, String secretKey) {
//    boolean flag = false;
//    try {
//      logger.info("Logging into application.");
//      Reporter.log("Enter User Name");
//      reuse.enterText(rpObjects.getElement(loginUsername), Username);
//      Reporter.log("Enter Password");
//      reuse.enterText(rpObjects.getElement(loginPassword), Password);
//      Reporter.log("Click Submit");
//      reuse.click(rpObjects.getElement(loginSubmit));
//      Reporter.log("Verify Dashboard UI");
//      reuse.click(rpObjects.getElement(dashboardMenu));
//      // if ((!secretKey.equalsIgnoreCase("NA"))) {
//      //   if (reuse.isElementPresent(rpObjects.getElement(mfaVerificationCodeInput))) {
//      //     reuse.enterText(
//      //         rpObjects.getElement(mfaVerificationCodeInput), getTheTOTPCode(secretKey.toString()));
//      //     reuse.click(rpObjects.getElement(mfaVerifyButton));
//      //     reuse.waitForElementVisible(rpObjects.getElement(mfaSuccessLogin));
//      //     reuse.waitForElementInvisible(rpObjects.getElement(mfaSuccessLogin));
//      //     //Thread.sleep(3000);
//      //   }
//      // }
//      reuse.waitForElementVisible(rpObjects.getElement(DBLabelHeading));
//      flag =
//          reuse.isElementPresent(rpObjects.getElement(DBLabelHeading))
//              && reuse.isElementPresent(rpObjects.getElement(DBLinkLogout));
//    } catch (Exception Ex) {
//      logger.error("Exception occurred while logging into application: " + Ex.getMessage());
//      throw new AssertionError("Unable to login the opeartion!", Ex);
//    }
//
//    return flag;
//  }
}

//  public boolean logOutFromApplication() {
//    boolean flag = false;
//    try {
//      reuse.click(rpObjects.getElement(dashboardMenu));
//      logger.info("Logout from the application.");
//      Reporter.log("Click Logout");
//      reuse.click(rpObjects.getElement(logout));
//      Reporter.log("Verify Login Page");
//      flag =
//          reuse.isElementPresent(rpObjects.getElement(loginUsername))
//              && reuse.isElementPresent(rpObjects.getElement(loginPassword));
//    } catch (Exception Ex) {
//      logger.error("Exception occurred while logging out from the application: " + Ex.getMessage());
//    }
//    return flag;
//  }

  
  
  