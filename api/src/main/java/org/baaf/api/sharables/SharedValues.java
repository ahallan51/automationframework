package org.baaf.api.sharables;

import java.util.HashMap;

public class SharedValues implements Shareable{
	
	HashMap<String, String> valuesList;

	public SharedValues() {
		this.valuesList = new HashMap<String, String>();
	}
	
	@Override
	public boolean exists(String strName) {
		return valuesList.containsKey(strName);
	}

	@Override
	public String get(String strName) {
		return valuesList.get(strName);
	}

	@Override
	public boolean set(String strName, String strValue) {
		boolean hasValue = true;
		
		if(!exists(strName)) {
			valuesList.put(strName,strValue);
		}
		else {
			hasValue = false;
		}
		return hasValue;
	}
	
	@Override
	public String toString() {
		return valuesList.toString();
	}
}
