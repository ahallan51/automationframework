package org.baaf.api.requests;

import java.util.HashMap;
import org.baaf.api.APIHeaders;
import org.baaf.api.APIMethods;
import org.baaf.api.APIResponse;
import org.baaf.api.sharables.Shareable;
import org.baaf.utilities.json.JsonHelper;

public class GETRequest extends APIRequest {
	HashMap<String,String> erDataSet;

    public GETRequest(APIMethods amMethod, JsonHelper jsAPIDetails, Shareable shVariables) {
        super(amMethod,jsAPIDetails,shVariables);
    }

    @Override
    public APIResponse execute() {
        APIResponse arAPIResponse = new APIResponse(amMethod);
        System.out.println(baseURI + strAPIPath);
        System.out.println(" Header " + requestSpecs.toString());
        arAPIResponse.setResponse(requestSpecs.get(strAPIPath));
        return arAPIResponse;
    }

    @Override
    public void prepareAPI(APIHeaders ahAPIHeader, HashMap<String,String> erDataSet) {
    	//GetRequest = given();
    	this.erDataSet= erDataSet;
   	requestSpecs = given();
        strAPIPath = updateURIParams(strAPIPath, erDataSet);
        requestSpecs.headers(createHeaders(erDataSet).getHeaderMap());
        //GetRequest.config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("application/x-amz-json-1.1", ContentType.TEXT)));
       // System.out.println("req" +req.toString());
    }
}   