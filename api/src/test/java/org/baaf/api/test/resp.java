package org.baaf.api.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class resp {

    @Test
    public void get_employee_details() {

        // String url = "https://www.googleapis.com/books/v1/volumes?q=turing";

        // Response getResponse(RequestSpecification,String)

        String url = "https://reqres.in/api/users?page=2";
        Response res = given().when().get(url);

        System.out.println(res.contentType());

        System.out.println(res.asString());

        System.out.println(res.prettyPrint());

        System.out.println(res.statusLine());

        System.out.println(res.getTime());

        System.out.println(res.statusCode());

        // Assert.assertEquals(res.path("first_name", "Michael"));

        System.out.println(res.then().body("email", equalTo("michael.lawson@reqres.in")));

    }

}