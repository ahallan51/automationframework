package org.baaf.api;

public enum APIMethods {
    GET, POST, PUT, DELETE, UPDATE, OPTIONS, UNDEFINED,PATCH;
}