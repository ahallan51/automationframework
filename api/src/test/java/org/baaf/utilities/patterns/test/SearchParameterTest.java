package org.baaf.utilities.patterns.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.baaf.utilities.patterns.SearchParameters;
import org.testng.annotations.Test;

@Test
public class SearchParameterTest {
    
    @Test
    public void testSearchPatter(){
        SearchParameters search = new SearchParameters(null);
        ArrayList<String> listParams = search.getParameterList("${Contet-Type}");
        for (String string : listParams) {
            System.out.println("Param: " + string);
        }
    }

    @Test
    public void testSearchPattern(){
        SearchParameters search = new SearchParameters(null);
        HashMap<String,String> valuesMap= new HashMap<>();
        valuesMap.put("Parameters", "String");
        valuesMap.put("EASY", "Trivial");
        String listParams = search.updatedParameters("Testing update ${Parameters} to data for API test execution. THis is ${EASY} Task?",valuesMap);
        System.out.println("UPdated list ::" + listParams ) ;
    }

    @Test
    public void testSearchinHashMap(){
        SearchParameters search = new SearchParameters(null);
        HashMap<String,String> valuesMap= new HashMap<>();
        valuesMap.put("Parameters", "String");
        valuesMap.put("EASY", "Trivial");
        HashMap<String,String> update= new HashMap<>();
        update.put("Parameters", "${Parameters}");
        update.put("EASY", "Trivial");
        HashMap<String,String> listParams = search.updateParameters(update,valuesMap);
        Set<String> keys = listParams.keySet();

        for (String string : keys) {
            System.out.println("Key : " + string + " Value:" + listParams.get(string));
        }
        
    }

    @Test
    public void testJSONString(){
        String JsonValues = "{" +
            "\"Name\": \" Value\","+
            "\"Key\": \"${Value}\","+
        "}";
        SearchParameters search = new SearchParameters(null);
        HashMap<String,String> valuesMap= new HashMap<>();
        valuesMap.put("Parameters", "String");
        valuesMap.put("EASY", "Trivial");
        HashMap<String,String> update= new HashMap<>();
        update.put("Parameters", "${Parameters}");
        update.put("Value", "Trivial");
        System.out.println(search.getParameterList(JsonValues));
        System.out.println(search.updatedParameters(JsonValues,update));

    }
@Test
public void URLUpdate(){
	  String JsonValues = "{" +
	            "\"Name\": \" Value\","+
	            "\"Key\": \"${Value}\","+
	        "}";
	        SearchParameters search = new SearchParameters(null);
	        HashMap<String,String> valuesMap= new HashMap<>();
	        valuesMap.put("Parameters", "String");
	        valuesMap.put("EASY", "Trivial");
	        HashMap<String,String> update= new HashMap<>();
	        update.put("Parameters", "${Parameters}");
	        update.put("Value", "Trivial");
	        System.out.println(search.getParameterList(JsonValues));
	        System.out.println(search.updatedParameters(JsonValues,update));
}
}