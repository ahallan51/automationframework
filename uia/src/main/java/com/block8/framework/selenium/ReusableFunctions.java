package com.block8.framework.selenium;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

public class ReusableFunctions {

  WebDriver driver;
  Properties prop;

  public ReusableFunctions(WebDriver driver, Properties prop) {
    this.driver = driver;
    this.prop = prop;
  }

  private Logger logger = Logger.getLogger(ReusableFunctions.class.getName());
  private int timeout;

  private int timeoutValue() {
    try {
      timeout = Integer.parseInt(prop.getProperty("Timeout"));
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Getting Timeout Property.");
    }
    return timeout;
  }

  public boolean waitForElementVisible(By locator) {
    WebDriverWait wait = new WebDriverWait(driver, timeoutValue());
    boolean flag = true;
    try {
      wait.ignoring(StaleElementReferenceException.class)
          .until(ExpectedConditions.visibilityOfElementLocated(locator));
    } catch (Exception Ex) {
      flag = false;
      logger.error("Exception Occurred While Locating The Element: " + Ex.getMessage());
      throw new AssertionError("Element did not found!");
    }
    return flag;
  }

  public boolean waitForElementInvisible(By locator) {
    WebDriverWait wait = new WebDriverWait(driver, timeoutValue());
    boolean flag = true;
    try {
      wait.ignoring(StaleElementReferenceException.class)
          .until(ExpectedConditions.invisibilityOfElementLocated(locator));
    } catch (Exception Ex) {
      flag = false;
      logger.error("Exception Occurred in waitForElementInvisible method: " + Ex.getMessage());
      throw new AssertionError("Failed while waiting for element to be invisible!");
    }
    return flag;
  }

  public boolean waitForElementVisible(WebElement element) {
    WebDriverWait wait = new WebDriverWait(driver, timeoutValue());
    boolean flag = true;
    try {
      wait.ignoring(StaleElementReferenceException.class)
          .until(ExpectedConditions.visibilityOf(element));
    } catch (Exception Ex) {
      flag = false;
      logger.error(
          "Exception Occurred While Locating The Element in waitForElementVisible method: "
              + Ex.getMessage());
      throw new AssertionError("Failed while waiting for element to become visible.");
    }
    return flag;
  }

  public boolean waitForElementPresent(By locator) {
    WebDriverWait wait = new WebDriverWait(driver, timeoutValue());
    boolean flag = true;
    try {
      wait.ignoring(StaleElementReferenceException.class)
          .until(ExpectedConditions.presenceOfElementLocated(locator));
    } catch (Exception Ex) {
      flag = false;
      logger.error(
          "Exception Occurred While Locating The Element in waitForElementPresent method: "
              + Ex.getMessage());
      throw new AssertionError("Failed while waiting for presence of element");
    }
    return flag;
  }

  public boolean waitForElementsPresent(By locator) {
    WebDriverWait wait = new WebDriverWait(driver, timeoutValue());
    boolean flag = true;
    try {
      wait.ignoring(StaleElementReferenceException.class)
          .until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    } catch (Exception Ex) {
      flag = false;
      logger.error(
          "Exception Occurred While Locating The Element in waitForElementsPresent method: "
              + Ex.getMessage());
      throw new AssertionError("Failed while waiting for elements to be present");
    }
    return flag;
  }

  public boolean isElementPresent(By locator) {
    WebDriverWait wait = new WebDriverWait(driver, timeoutValue());
    boolean flag = true;
    try {
      wait.ignoring(StaleElementReferenceException.class)
          .until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    } catch (Exception Ex) {
      flag = false;
      logger.error("Element " + locator.toString() + " \n Could not be found. " + Ex.getMessage());
      throw new AssertionError(
          "ReusableFunctions.isElementPresent: Failed while checking presence of element");
    }
    return flag;
  }

  public Integer numberOfElementsLocated(By locator) {
    int num = 0;
    try {
      waitForElementsPresent(locator);
      num = (driver.findElements(locator)).size();

    } catch (Exception Ex) {
      logger.error(
          "Exception Occurred While Locating The Elements numberOfElementsLocated method: "
              + Ex.getMessage());
      throw new AssertionError("Failed while checking number of elements present");
    }
    return num;
  }

  public boolean verifyElementsLocated(ArrayList<By> arrayList) {
    boolean flag = true;
    try {
      for (By locator : arrayList) {
        if (waitForElementVisible(locator)) {
          logger.info(driver.findElement(locator).toString() + ": element is displayed.");
        } else {
          logger.error(driver.findElement(locator).toString() + ": element isn't displayed.");
          flag = false;
        }
      }
    } catch (Exception Ex) {
      flag = false;
      logger.error(
          "Exception Occurred While Locating The Elements in verifyElementsLocated method: "
              + Ex.getMessage());
      throw new AssertionError("VerifyElementsLocated: Failed while verifying elements on page");
    }
    return flag;
  }

  public boolean isElementChecked(By locator) {
    boolean flag = true;
    try {
      if (waitForElementPresent(locator)) {
        if (!driver.findElement(locator).isSelected()) {
          flag = false;
        }
        logger.info(driver.findElement(locator).toString() + ": element is selected.");
      }
    } catch (Exception Ex) {
      flag = false;
      logger.error("Exception Occurred While Checking the Element Selected: " + Ex.getMessage());
      throw new AssertionError("isElementChecked: Failed while checking if element is checked");
    }
    return flag;
  }

  public boolean isElementEnabled(By locator) {
    boolean flag = true;
    try {
      if (waitForElementPresent(locator)) {
        if (!driver.findElement(locator).isEnabled()) {
          flag = false;
        }
        logger.info(driver.findElement(locator).toString() + ": element is enabled.");
      }
    } catch (Exception Ex) {
      flag = false;
      logger.error(
          "Exception Occurred While Checking the Element Enabled Status: " + Ex.getMessage());
      throw new AssertionError("isElementEnabled: Failed while checking if element is enabled.");
    }
    return flag;
  }

  public boolean waitForElementClickable(By locator) {
    WebDriverWait wait = new WebDriverWait(driver, timeoutValue());
    boolean flag = true;
    try {
      wait.ignoring(StaleElementReferenceException.class)
          .until(ExpectedConditions.elementToBeClickable(locator));
    } catch (Exception Ex) {
      flag = false;
      logger.error("Exception Occurred While Locating The Element: " + Ex.getMessage());
      throw new AssertionError(
          "waitForElementClickable: Failed while checking if element is clickable.");
    }
    return flag;
  }

  public void enterText(By locator, String value) {
    try {
      if (waitForElementVisible(locator)) {
        WebElement element = driver.findElement(locator);
        element.clear();
        element.sendKeys(value);
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Entering The Text: " + Ex.getMessage());
      throw new AssertionError("enterText: Failed while attempting to enter text");
    }
  }

  public void forceClear(By locator) {
    try {
      if (waitForElementVisible(locator)) {
        WebElement element = driver.findElement(locator);
        element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        element.sendKeys(Keys.BACK_SPACE);
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While force cleaning the text area : " + Ex.getMessage());
      throw new AssertionError("forceClear: Failed while force clear");
    }
  }

  public void pressEnterKey(By locator) {
    try {
      if (waitForElementVisible(locator)) {
        Thread.sleep(500);
        WebElement element = driver.findElement(locator);
        element.sendKeys(Keys.ENTER);
        Thread.sleep(500);
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Sending Enter Key" + Ex.getMessage());
      throw new AssertionError("pressEnterKey: Failed while trying to send ENTER key");
    }
  }

  public void dropdownSelect(By locator, String value) {
    try {
      if (waitForElementVisible(locator)) {
        WebElement element = driver.findElement(locator);
        Select select = new Select(element);
        select.selectByVisibleText(value);
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Selecting an Option: " + Ex.getMessage());
      throw new AssertionError("dropdownSeleect: Failed while trying to select a dropdown");
    }
  }

  public void click(By locator) throws Exception {
    try {
      if (waitForElementClickable(locator)) {
        WebElement element = driver.findElement(locator);
        element.click();
        Reporter.log("Click on element - " + locator.toString());
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Clicking The Element: " + Ex.getMessage());
      Reporter.log("Exception Occurred While Clicking The Element: " + Ex.getMessage());
      throw new Exception("Failed while waiting for element.." + locator.toString());
    }
  }

  public void selectDropdownValue(By locator, String value) {
    try {
      if (waitForElementVisible(locator)) {
        click(locator);
        click(By.xpath("//div[text()='" + value + "']"));
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Selecting an Option: " + Ex.getMessage());
      throw new AssertionError("selectDropDownValue: Failed while selecting a dropdown value");
    }
  }

  public void moveToElementAndClick(By locator) {
    try {
      if (waitForElementClickable(locator)) {
        WebElement element = driver.findElement(locator);

        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("arguments[0].scrollIntoView(true)", element);

        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().build().perform();
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Clicking The Element: " + Ex.getMessage());
      throw new AssertionError(
          "moveToElementAndClick: Failed while move mouse pointer and click element");
    }
  }

  public List<WebElement> getElements(By locator) {
    return driver.findElements(locator);
  }

  public String getTextByAttributeValue(By locator) {
    String text = null;
    try {
      if (waitForElementPresent(locator)) {
        WebElement element = driver.findElement(locator);
        text = element.getAttribute("value");
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Getting The Text: " + Ex.getMessage());
      throw new AssertionError(
          "getTextByAttributeValue: Failed while trying to getText from textbox.");
    }
    return text;
  }

  public String getTextByInnerText(By locator) {
    String text = null;
    try {
      if (waitForElementPresent(locator)) {
        WebElement element = driver.findElement(locator);
        text = element.getText();
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Getting The Text: " + Ex.getMessage());
      throw new AssertionError("Unable to perform the opeartion!");
    }
    return text;
  }

  public WebElement getElement(By locator) {
    WebElement element = null;
    try {
      element = driver.findElement(locator);

    } catch (Exception Ex) {
      logger.error("Exception Occurred While Getting The Element: " + Ex.getMessage());
      throw new AssertionError("getElement: Failed in getElement");
    }
    return element;
  }

  public String getSelectedValueOfDropdown(By locator) {
    String selectedValue = null;
    try {
      Select select = new Select(getElement(locator));
      WebElement option = select.getFirstSelectedOption();
      selectedValue = option.getText();

    } catch (Exception Ex) {
      logger.error("Exception Occurred While Retriving The Text: " + Ex.getMessage());
      throw new AssertionError("getSelectedValueOfDropdown: Failed while getting an element");
    }
    return selectedValue;
  }

  public String takeScreenShot(String screenshotName) {
    String destination = null;
    String strFolderPath = null;
    try {
      String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
      TakesScreenshot ts = (TakesScreenshot) driver;
      File source = ts.getScreenshotAs(OutputType.FILE);

      strFolderPath =
           "/Screenshots/" + screenshotName + dateName + ".png";
      destination = System.getProperty("user.dir") +"/ExtentReports" + strFolderPath;
      File finalDestination = new File(destination);
      FileUtils.copyFile(source, finalDestination);

      destination = finalDestination.getAbsolutePath();
      logger.info("Saving screenshot to failed repo: " + destination);

    } catch (Exception Ex) {
      logger.error("Exception Occurred While Getting The Text: " + Ex.getMessage());
      throw new AssertionError("takeScreenShot: Failed while saving screenshot");
    }
    return "." + strFolderPath;
  }

  public boolean verifyTextMatch(String actualText, String expectedText) {
    boolean flag = false;
    try {
      logger.info("Actual Text From Application Web UI --> :: " + actualText);
      logger.info("Expected Text From Application Web UI --> :: " + expectedText);

      if (actualText.equals(expectedText)) {
        logger.info("### VERIFICATION TEXT MATCHED !!!");
        flag = true;
        Reporter.log("Expected: " + expectedText + " - \n Actual: " + actualText);
      } else {
        logger.error("### VERIFICATION TEXT DOES NOT MATCHED !!!");
        Reporter.log("Expected: " + expectedText + " - \n Actual: " + actualText);
      }

    } catch (Exception Ex) {
      logger.error("Exception Occurred While Verifying The Text Match: " + Ex.getMessage());
      throw new AssertionError("verifyTextMatch: Failed while verification of text match.");
    }
    return flag;
  }

  public boolean assertTextMatch(String actualText, String expectedText) {
    boolean flag = false;
    try {
      logger.info("Actual Text From Application Web UI --> :: " + actualText);
      logger.info("Expected Text From Application Web UI --> :: " + expectedText);

      if (actualText != null && actualText.equals(expectedText)) {
        logger.info("### VERIFICATION TEXT MATCHED !!!");
        flag = true;
        Reporter.log("Expected: " + expectedText + " - \n Actual: " + actualText);
      } else {
        logger.error("### VERIFICATION TEXT DOES NOT MATCHED !!!");
        Reporter.log("Expected: " + expectedText + " - \n Actual: " + actualText);
        throw new AssertionError("Expected: " + expectedText + "  \n  Found : " + actualText);
      }
    } catch (Exception Ex) {
      logger.error("Exception Occurred While Verifying The Text Match: " + Ex.getMessage());
      throw new AssertionError("assertTextMatch: Failed asserting text match");
    }
    return flag;
  }

  public boolean hoverOverElement(WebElement element) {
    Reporter.log("Hover on the element.");
    MouseActions mouse = new MouseActions(driver);
    mouse.hoverOverElement(element);
    return true;
  }

  public String getTitle() {
    Reporter.log("Fetch Title of the page.");
    return driver.getTitle();
  }

  public boolean switchWindow(String strWindowTitle) {
    Set<String> lstWindows = driver.getWindowHandles();
    for (String string : lstWindows) {
      System.out.println("Open windows list :: " + string);
      driver.switchTo().window(string);
      if (driver.getTitle().equalsIgnoreCase(strWindowTitle)) {
        Reporter.log("Switch to window by title : " + strWindowTitle);
        return true;
      }
    }
    throw new AssertionError(
        "Unable to find window with title : " + strWindowTitle,
        new Exception("Unable to find window with title : " + strWindowTitle));
  }

  public void openURL(String url) {
    try {
      driver.navigate().to(url.trim());
      Reporter.log("\n Successfully navigated to URL: " + url);
    } catch (Exception e) {
      Reporter.log("\n Unable to navigate to URL: " + url + ". Exception = " + e.getMessage());
      Assert.fail(e.getMessage());
    }
  }

  public ArrayList<String> getWindows() {
    // TODO Auto-generated method stub
    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
    return tabs;
  }

  public void closeTab() {
    driver.close();
  }
  
  public void waitFixed() {
	  try {
		  Thread.sleep(5000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
	}
  }
}

