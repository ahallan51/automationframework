package com.block8.framework.repositories.test;

import java.io.IOException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.block8.framework.repositories.Repository;

@Test
public class RepositoryTest {
  Repository rpObjectRepository;
  Repository rpObjectRepositorySecondInstance;
  String strLoginRepository =
      System.getProperty("user.dir") + "/src/test/resource/Repository/PlayChipCentralRepo.txt";
  String strDashboardRepository =
      System.getProperty("user.dir") + "/src/test/resource/Repository/PlayChipDashboard.txt";

  @Test(priority = 1)
  public void checkReadRepository() {
    System.out.println(rpObjectRepository.getElement("Login.Username"));
  }

  @Test(priority = 2)
  public void cehckExistingRepository() {
    try {
      rpObjectRepositorySecondInstance = Repository.getRepositoryInstance(strDashboardRepository);
    } catch (IOException e) {
      e.printStackTrace();
    }

    System.out.println(
        rpObjectRepositorySecondInstance.isElementDefined("Dashboard.Balance.Label.Heading"));
    System.out.println(rpObjectRepository.isElementDefined("Dashboard.Balance.Label.Heading"));
    System.out.println(
        "Does contain login element after udpate ::"
            + rpObjectRepository.isElementDefined("Login.Username"));
  }

  @BeforeClass
  public void beforeMethod() {
    try {
      rpObjectRepository = Repository.getRepositoryInstance(strLoginRepository);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @AfterMethod
  public void afterMethod() {}

  @AfterSuite
  public void afterSuite() {}
}
