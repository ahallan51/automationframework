package com.block8.framework.repositories;

import java.io.IOException;

import org.openqa.selenium.By;

public class Repository {

  private static Repository rpObjectRepository;

  private String strRepositoryFileName;
  private ObjectMapper omObjectFetcher;

  private Repository(String strRepositoryFileName) {
    this.strRepositoryFileName = strRepositoryFileName;
    try {
      omObjectFetcher = new ObjectMapper(strRepositoryFileName);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private boolean isRepositoryStoreChanged(String strRepositoryFileName) {

    if (this.strRepositoryFileName.equalsIgnoreCase(strRepositoryFileName)) {
      return false;
    } else {
      this.strRepositoryFileName = strRepositoryFileName;
      return true;
    }
  }

  public By getElement(String strElementName) {
    return omObjectFetcher.getElementLocator(strElementName);
  }

  public boolean isElementDefined(String strElementName) {
    return omObjectFetcher.isElementDefined(strElementName);
  }

  public String getElementIdentifierRaw(String strElementName) {
    return omObjectFetcher.getRawLocator(strElementName);
  }

  public static synchronized Repository getRepositoryInstance(String strRepositoryFileName)
      throws IOException {
    if (rpObjectRepository == null) {
      rpObjectRepository = new Repository(strRepositoryFileName);
    }
    if (rpObjectRepository.isRepositoryStoreChanged(strRepositoryFileName)) {
      rpObjectRepository.omObjectFetcher.updateRepositoryStore(strRepositoryFileName);
    }

    return rpObjectRepository;
  }
}
