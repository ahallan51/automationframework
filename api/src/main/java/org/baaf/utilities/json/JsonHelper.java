package org.baaf.utilities.json;

import java.io.File;
import java.util.HashMap;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.internal.JsonReader;

public class JsonHelper {
    
    JsonReader jsonReader;
    DocumentContext jsonPath;
    LoadfromFile loadfromFile;

    private JsonHelper(File strFilePath){
        loadfromFile = new LoadfromFile(strFilePath);
        loadfromFile.loadJSON();
        jsonReader = loadfromFile.getValue();
        jsonPath = JsonPath.parse(jsonReader.jsonString());
    }

    private JsonHelper(String strJSON){
        loadfromFile.parseFromString(strJSON);
    }

    public static JsonHelper parseJsonString(String strJSONContent){
        return new JsonHelper(strJSONContent);
    }

    public static JsonHelper parseJsonFile(String strJSONFilePath){

        File jsonFile = new File(strJSONFilePath);
        return new JsonHelper(jsonFile);
    }

    public String searchValue(String strSearchValue){
        return jsonReader.read(strSearchValue);
    }

    public HashMap<String,String> readComplexValue(String setSearchValue){
        return jsonReader.read(setSearchValue);
    }

    public String readComplexObjectAsString(String setSearchValue){
        String result = jsonPath.read("$.."+setSearchValue).toString();
        result = result.substring(1, result.length()-1);
        return result;
    }

    @Override
    public String toString(){
        return jsonReader.jsonString();
    }

}