package org.baaf.api.test;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class statuscode {

    @Test(priority = 1)
    public void Single_Content() {

        // // Verifying single content in Response-Body
        given().when().get("https://reqres.in/api/users/2").then().statusCode(200)
                .body("data.first_name", equalTo("Janet")).log().all();

    }

    @Test(priority = 2)

    public void multiple_content() {

        // Verifying multiple content in Response Body
        given().when().get("https://reqres.in/api/users?page=2").then().assertThat().statusCode(200)
                .body("data.first_name", hasItems("Rachel", "George")).log().all();

        // .body("id", equalTo(testMovie.getId()))
        // .body("name", equalTo(testMovie.getName()))
        // .body("synopsis", notNullValue());

    }

    // baseURI = "http://www.postalpincode.in";
    // // Status is a tag name and value of it is success and validating(by using
    // // assert) that value of status is success or not
    // // Is.is is coming from hamcrest.core(useful to validate these kind of
    // things)
    // // To see all the data we can use log()all()
    // given().get("/api/pincode/560100").then().assertThat().body("Status",
    // Is.is("Success")).log().all();

    // // Now,Will be Validating that postOffice name=Electronics City or not. need
    // to
    // // mention the element which you want to validate.

    // // PostOff-Parent one , NAme-Childe one and index-[0]
    // given().get("/api/pincode/560100").then().assertThat().body("PostOffice.Name[0]",
    // Is.is("ElectronicsCity"))
    // .log().all();

}
