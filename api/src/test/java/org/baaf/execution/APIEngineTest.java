package org.baaf.execution;

import org.testng.annotations.Test;

@Test   
public class APIEngineTest {

    @Test
    public void testExecuteAPI() {
        APIEngine apiEngine;
        try {
            apiEngine = new APIEngine();
            apiEngine.setupReporting();
            apiEngine.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}