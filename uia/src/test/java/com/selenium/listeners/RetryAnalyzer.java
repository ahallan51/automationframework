package com.selenium.listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import com.block8.framework.selenium.TestBase;

public class RetryAnalyzer implements IRetryAnalyzer {
  static TestBase testBase = new TestBase();

  private int counter = 0;
  private int retryLimit = Integer.parseInt(testBase.prop.getProperty("Limit"));

  public boolean retry(ITestResult result) {
    if (counter < retryLimit) {
      counter++;
      return true;
    }
    return false;
  }
}
