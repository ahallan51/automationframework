package com.selenium.listeners;

import com.block8.framework.selenium.TestBase;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class Transformer implements IAnnotationTransformer {

  TestBase testBase = new TestBase();
  //static HashMap<String, HashMap<String, String>> runmodeData = DataReader.testDataMappedToTestName(testBase.prop.getProperty("TestDataExcelFileName"), testBase.prop.getProperty("RunModeSheetName"));

  public void transform(
      ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
    //annotation.setEnabled(DataReader.isRunnable(testMethod.getName(), runmodeData));
    annotation.setRetryAnalyzer(RetryAnalyzer.class);
  }
}
