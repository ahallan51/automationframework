package org.baaf.api.requests;

import java.util.HashMap;
import org.baaf.api.APIHeaders;
import org.baaf.api.APIMethods;
import org.baaf.api.APIResponse;
import org.baaf.api.IAPI;
import org.baaf.api.PayLoadJson;
import org.baaf.api.authentication.Auth;
import org.baaf.api.sharables.Shareable;
import org.baaf.utilities.json.JsonHelper;
import org.baaf.utilities.patterns.SearchParameters;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public abstract class APIRequest extends RestAssured implements IAPI {

    APIMethods amMethod;
    APIHeaders ahAPIHeaders;
    String strAPIPath;
    JsonHelper jsAPIDetails;
    final private String strParamPattern = "\\$\\{.[^\\{}]*\\}";
    final private String strVairablePattern = "\\&\\{.[^\\{}]*\\}";
    SearchParameters spUPdateValues, spUpdateVariables;
    PayLoadJson pljRequestPayload;
    Auth encode;
    Shareable shVariables;
    RequestSpecification requestSpecs;

    public APIRequest(APIMethods amMethod, JsonHelper jsAPIDetails, Shareable shVariables) {
        this.amMethod = amMethod;
        this.jsAPIDetails = jsAPIDetails;
        this.spUPdateValues = new SearchParameters(strParamPattern);
        this.spUpdateVariables = new SearchParameters(strVairablePattern);
        this.shVariables = shVariables;
        requestSpecs = given();
    }

    @Override
    public void setAPIMethod(APIMethods amAPIMethod) {
        this.amMethod = amAPIMethod;
    }

    @Override
    public void setBaseURL(String strURL) {
        baseURI = strURL;
    }

    public void setURI(String strURL) {
        this.strAPIPath = strURL;
    }

    @Override
    public String getAPIURI(){
        return strAPIPath;
    }
    
    public void setencoding(RequestSpecification request ) {
    	  this.encode = new Auth(request);
    	encode.encoderType(jsAPIDetails.searchValue("Authentication.encoding"));
    	System.out.println("EncodingFromTemplate" +jsAPIDetails.searchValue("Authentication.encoding"));
    }

    abstract public APIResponse execute();

    public APIHeaders createHeaders(HashMap<String,String> erDataStuffer){
        ahAPIHeaders = new APIHeaders(jsAPIDetails.readComplexValue("headers"));
        //System.out.println("Ankita"+ahAPIHeaders.toString());
        spUPdateValues.updateParameters(ahAPIHeaders.getHeaderMap(), erDataStuffer);
        spUpdateVariables.updateVariables(ahAPIHeaders.getHeaderMap(), shVariables);
        System.out.println(" Header " + ahAPIHeaders.getHeaderMap().toString());
        return ahAPIHeaders;
    }

    public String createRequestPayLoad(HashMap<String, String> erDataStuffer){
        String payLoadJSON = jsAPIDetails.readComplexObjectAsString("Payload");
		payLoadJSON = spUPdateValues.updatedParameters(payLoadJSON, erDataStuffer);
		payLoadJSON = spUpdateVariables.updatedVariables(payLoadJSON, shVariables);
        pljRequestPayload = new PayLoadJson(payLoadJSON);
       // System.out.println("Payload : " + payLoadJSON);
        // pljRequestPayload.getUpdatedPayLoad(erDataStuffer);
        return payLoadJSON;
    }

    public String updateURIParams(String strURIPath, HashMap<String, String> erDataStuffer){
       String Variables=strURIPath;
       Variables = spUpdateVariables.updatedVariables(Variables,shVariables);
       System.out.println("variables" +Variables);
       
        return spUPdateValues.updatedParameters(Variables, erDataStuffer);
        
        
    }
    
    public String updateURIVariables(String strURIPath, Shareable sharedValues){
        return spUpdateVariables.updatedVariables(strURIPath, sharedValues);
    }
}