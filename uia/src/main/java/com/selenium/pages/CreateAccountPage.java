package com.selenium.pages;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.jboss.aerogear.security.otp.Totp;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.block8.framework.repositories.Repository;
import com.block8.framework.selenium.ReusableFunctions;



public class CreateAccountPage {
	 private Logger logger;
	  ReusableFunctions reuse;
	  Repository rpObjects;
	 
	  private String ClickAccountCreate = "Click.Createaccount";
	  private String FirstName = "Enter.FirstName";
	  private String lastName = "Enter.LastName";
	  private String Email = "Enter.Email";
	  private String PhoneNumber = "Enter.PhoneNumber";
	  private String CompanyName = "Enter.CompanyName";
	  private String ACN = "Enter.ACN";
	  private String CompanyType = "Enter.CompanyType";
	  private String DropdownClick = "Enter.CompanyClick";
	  private String DropdownSelect= "Enter.CompanySlect";
	  private String Password = "Enter.Password";
	  private String ConfirmPassword = "Enter.ConfirmPassword";
	  private String TermAndCondition = "Click.Termandcondition";
	  private String CreateAccountbutton = "Click.CreateAccount";
	  
	  
	 
	  public CreateAccountPage (WebDriver driver, Properties prop, Repository rpObjects) {
	    super();
	    reuse = new ReusableFunctions(driver, prop);
	    logger = Logger.getLogger(LoginPage.class.getName());
	    this.rpObjects = rpObjects;
	  }

	  public boolean AccountCreation(String firstName, String lastname, String email ,String phonenumber , String Companyname , String acn , String CT , String password) throws Exception {
		    boolean flag = false;
		   try {
		      logger.info("Create Account");
		      Reporter.log("Click create account");
		      reuse.click(rpObjects.getElement(ClickAccountCreate));
		      Reporter.log("EnterUserName");
		      reuse.enterText(rpObjects.getElement(FirstName), firstName);
		      Reporter.log("EnterLastName");
		      reuse.enterText(rpObjects.getElement(lastName), lastname);
		      Reporter.log("Email");
		      reuse.enterText(rpObjects.getElement(Email), email);
		      Reporter.log("PhoneNumber");
		      reuse.enterText(rpObjects.getElement(PhoneNumber), phonenumber);
		      Reporter.log("CompanyName");
		      reuse.enterText(rpObjects.getElement(CompanyName), Companyname);
		      Reporter.log("ACN");
		      reuse.enterText(rpObjects.getElement(ACN), acn);
		      Reporter.log("CompanyType");
		      reuse.click(rpObjects.getElement(DropdownClick));
		      reuse.click(rpObjects.getElement(DropdownSelect));
		     // reuse.dropdownIndex(rpObjects.getElement(CompanyType),1);
		     
		      Reporter.log("Password");
		      reuse.enterText(rpObjects.getElement(Password), password);
		      Reporter.log("Confirmpassword");
		      reuse.enterText(rpObjects.getElement(ConfirmPassword ), password);
		      Reporter.log("T&C");
		      //Thread.sleep(3000);
		      reuse.click(rpObjects.getElement(TermAndCondition));
		      
		      Reporter.log("CreateAccountbutton");
		      reuse.click(rpObjects.getElement(CreateAccountbutton));
		      
			/*
			 * reuse.waitForElementVisible(rpObjects.getElement(DBLabelHeading)); flag =
			 * reuse.isElementPresent(rpObjects.getElement(DBLabelHeading)) &&
			 * reuse.isElementPresent(rpObjects.getElement(DBLinkLogout));
			 */
    } 
		      catch (Exception Ex) {
	      logger.error("Exception occurred while logging into application: " + Ex.getMessage());
	      throw new AssertionError("Unable to login the opeartion!", Ex);
		    }

		    return flag;
		  }


	 

}
