package org.baaf;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGFormatter implements ITestListener {
    
    @Override		
    public void onTestSuccess(ITestResult results){
         System.out.println("Test Result passed for : " + results.getName());
    }

    @Override		
    public void onTestFailure(ITestResult results){
        System.out.println("Test Failed for : " + results.getName());
    }

    @Override		
    public void onTestSkipped(ITestResult results){
        System.out.println("Test Skipped for name :" + results.getName());
    }
}