package com.block8.framework.data.test;

import java.io.IOException;
import java.util.Set;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.block8.framework.data.ExcelReader;
import com.block8.framework.data.PropertiesReader;

public class DataFeederTest {
  ExcelReader dfDataProvider;
  PropertiesReader prDataFeed;

  @Test
  public void f() {
    try {
      System.out.println("Executing test case");
      while (dfDataProvider.hasNext()) {
        
          dfDataProvider.readNextRow();
        } 
      }catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      System.out.println("Columns:: " + dfDataProvider.getData("ColumnName"));

    Set<Object> keys = prDataFeed.getRepositoryObject().keySet();

    for (Object string : keys) {
      System.out.println("Properties Key:: " + string.toString());
    }
  }

  @BeforeTest
  public void beforeTest() {
    try {
      dfDataProvider =
          ExcelReader.getInstance(
              System.getProperty("user.dir") + "/src/test/resource/TestData.xlsx", "Excel");
      prDataFeed =
          PropertiesReader.getInstance(
              System.getProperty("user.dir") + "/src/main/resource/config.properties");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @AfterTest
  public void afterTest() {
    System.out.println("Ending Test");
    try {
      dfDataProvider.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
