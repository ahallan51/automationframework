package org.baaf.api.test;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class APITest {
    @Test
    public void GetRequest() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";

        Response response = null;
        try {
            response = RestAssured.given().when().get("/employees");
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Response :" + response.asString());
        System.out.println("Response :" + response.getStatusCode());
        System.out.println(
                "Does Reponse contains 'employee_salary'? :" + response.asString().contains("employee_salary"));

    }

}
