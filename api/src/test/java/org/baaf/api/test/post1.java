package org.baaf.api.test;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class post1 {

    @Test(dataProvider = "dataforPost") // refrencing the data using DP
    public void Post_01() {

        // While creating a post Req we need a body and for that i have used
        // JSON-LIBRARY like simple JSON.
        // used the request variable to create my request.
        // created data which we can send along post data
        final JSONObject request = new JSONObject();
        request.put("email", "subinaverma7837@gmail.com");
        request.put("first_name", "subina");
        request.put("last_name", "verma");

        baseURI = "https://reqres.in/api/users";// Setting the path.

        // Here i am passing the type is of JSON-Type(Content-Type)
        // and the response is also accepted is of JSON-Type
        // in body i have used JSOn.String so that it converts into this format.
        // Add header stating the request bosy is json
        // Add the json to the body of request
        given().contentType(ContentType.JSON).accept(ContentType.JSON).header("Content-Type", "application/Json")
                .body(request.toJSONString()).when().post("/create").then().statusCode(201).log().all();

        // Map<String, Object> map = new HashMap<String, Object>();
        // map.put("name", "subina");
        // map.put("job", "testEnginner");
        // System.out.println(map);

        // JSONObject request = new JSONObject();

        // request.put("name", "subina");
        // request.put("job", "testEnginner");
        // //System.out.println(request);

        // given().header("Content-Type",
        // "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
        // .body(request.toJSONString()).when().post("https://reqres.in/api/users").then().statusCode(201);

        // or we can use this toJSONString- System.out.println(request.toJSONString());
    }

}
