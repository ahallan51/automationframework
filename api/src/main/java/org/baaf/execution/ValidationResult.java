package org.baaf.execution;

public class ValidationResult {
    
    private boolean boolFinalResult=false;
    private String strMessage;

    public ValidationResult(boolean boolFinalResult, String strMessage){
        this.boolFinalResult = boolFinalResult;
        this.strMessage = strMessage;
    }

    public ValidationResult(){}

    public void setMessage(String strMessage){
        this.strMessage = strMessage;
    }

    public void setFinalResult(boolean boolFinalResult){
        this.boolFinalResult = boolFinalResult;
    }

    public boolean getFinalResult(){
        return boolFinalResult;
    }

    public String getMessage(){
        return strMessage;
    }
}