package org.baaf.api.validators;

import java.util.HashMap;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.baaf.api.APIResponse;
import org.baaf.execution.APIEngine;
import org.baaf.reporting.ValidationReporter;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;
import com.block8.framework.selenium.ReusableFunctions;

import groovy.xml.MarkupBuilder;

public class ResponseValidator {
    
    APIResponse aResponse;
    HashMap<String, String> hmExpectedData, hmValidators;
    ValidationReporter vrResultLogger;

    public ResponseValidator(HashMap<String, String>  hmValidators, APIResponse aResponse){
        this.hmValidators = hmValidators;
        this.aResponse = aResponse;
        this.vrResultLogger = new ValidationReporter(APIEngine.extLogger);
    }

    public ResponseValidator(APIResponse aResponse){
        this.hmValidators = new HashMap<>();
        this.aResponse = aResponse;
    }

    public void setExpectedData(HashMap<String, String> hmExpectedData){
        this.hmExpectedData = hmExpectedData;
    }
    

    public void validateHTTPCode(int iHTTPCode){
        Assert.assertEquals(iHTTPCode, aResponse.getHTTPSATUSCODE(), "Actual value did not match expected");
    }

    public void validateResposeValue(String strColumnName, String strJSONPath, String strMessage) throws Exception {
        try{
        
        	System.out.println("strjsonpaath" +strJSONPath +"strcolumnname"+strColumnName);
        	APIEngine.logger.info("strjsonpaath" +strJSONPath +"strcolumnname"+strColumnName);
            String strActualValue = aResponse.readValue(strJSONPath);
            String strExpectedValue = hmExpectedData.get(strColumnName);
            vrResultLogger.assertResultsMatch(strColumnName, strActualValue, strExpectedValue);
        }catch(AssertionError aError){
            APIEngine.logger.error(aError.getMessage());
            APIEngine.logger.error("\n ============Stack Trace============ \n" + ArrayUtils.toString(aError.getStackTrace()));
            throw new Exception(aError.getMessage());
        }
    }

    public void validateResponses() throws Exception {
        Set<String> responses = hmValidators.keySet();
        for (String string : responses) {
            validateResposeValue(string, hmValidators.get(string), "Validation Failed: " + string );
        }
        
        
    }
    public void validateHTTPCode(String iHTTPCode){
        vrResultLogger.assertResultsMatch("HTTP Response Code :",iHTTPCode, String.valueOf(aResponse.getHTTPSATUSCODE()));
    }
}