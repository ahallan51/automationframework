package com.block8.framework.reporting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.block8.framework.data.PropertiesReader;

//OB: ExtentReports extent instance created here. That instance can be reachable by getReporter() method.

public class ExtentManager {

  private static ExtentReports extent;
  private static ExtentTest logger;
  static String workingDir = System.getProperty("user.dir");

  public ExtentManager(ExtentReports reports, String strFrameWorkRoot) {
    extent = reports;
    workingDir = strFrameWorkRoot;
  }

  public static synchronized ExtentReports getReporter() throws IOException {
    if (extent == null) {
      PropertiesReader prCOnfigurationSettings = PropertiesReader
          .getInstance(workingDir + "/uia/Reporting/configurations/config.properties");
      FileInputStream inputStream =
          new FileInputStream(new File(workingDir + "/uia/Reporting/configurations/extent-config.xml"));
      Path pExtentReportFolder = Paths.get(workingDir + "/ExtentReports/");

      if (Files.notExists(pExtentReportFolder)) {
        Files.createDirectories(pExtentReportFolder);
      }
      ExtentHtmlReporter htmlReporter =
          new ExtentHtmlReporter(
              workingDir
                  + "/ExtentReports/"
                  + prCOnfigurationSettings.getData("HostName")
                  + String.valueOf(Calendar.getInstance().getTime()).replace(":", "_")
                  + ".html");
      htmlReporter.loadConfig(inputStream);
      extent = new ExtentReports();
      extent.attachReporter(htmlReporter);
      extent.setSystemInfo("Host Name", prCOnfigurationSettings.getData("HostName"));
      extent.setSystemInfo("Environment", prCOnfigurationSettings.getData("Environment"));
      extent.setSystemInfo("User Name", prCOnfigurationSettings.getData("UserName"));
    }
    return extent;
  }

  public static synchronized ExtentTest getLogger(String testCaseName) {
    try {
      logger = getReporter().createTest(testCaseName);
    } catch (Exception Ex) {
      Ex.printStackTrace();
    }
    return logger;
  }
}
