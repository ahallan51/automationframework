package org.baaf.configurations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

public class Configurations {

    public HashMap<String, String> hmAPIDetails;

    public Configurations(String Path) {
        Properties file = new Properties();
        try {
            file.load(Configurations.class.getResourceAsStream(Path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        hmAPIDetails = new HashMap<>();
        Set<Object> list = file.keySet();
        for (Object string : list) {
            hmAPIDetails.put((String) string, file.getProperty((String) string));
        }
    }

    public HashMap<String, String> getHashMap() {
        return hmAPIDetails;
    }
}