package com.selenium.pages;

import java.util.Properties;
import com.block8.framework.repositories.Repository;
import com.block8.framework.selenium.ReusableFunctions;
import org.apache.log4j.Logger;
import org.jboss.aerogear.security.otp.Totp;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

public class LoginPage {
  private Logger logger;
  ReusableFunctions reuse;
  Repository rpObjects;

  private String loginUsername = "Login.Username";
  private String loginPassword = "Login.Password";
  private String loginSubmit = "Login.Submit";
  private String DBLabelHeading = "Dashboard.Balance.Label.Heading";
  private String DBLabelFree = "Dashboard.Balance.Label.Free";
  private String DBLabelPaid = "Dashboard.Balance.Label.Paid";
  private String PLAFBalance = "Dashboard.Balance.Value.Free";
  private String PLABalance = "Dashboard.Balance.Value.Paid";
  private String DBLinkLogout = "Dashboard.TopMenu.Link.Logout";
  private String mfaVerificationCodeInput = "Login.MFA.VerificationCode";
  private String mfaVerifyButton = "Login.MFA.VerifyButton";
  private String mfaSuccessLogin = "Login.MFA.SuccessLogin";
  private String logout = "Dashboard.Logout";
  private String dashboardMenu = "Page.Menu.Dashboard";

  public LoginPage(WebDriver driver, Properties prop, Repository rpObjects) {
    super();
    reuse = new ReusableFunctions(driver, prop);
    logger = Logger.getLogger(LoginPage.class.getName());
    this.rpObjects = rpObjects;
  }

  public void clickOnDashboardOption() {
    try {
      logger.info("Clicking on the Dashboard menu");
      reuse.waitForElementVisible(rpObjects.getElement(dashboardMenu));
      reuse.click(rpObjects.getElement(dashboardMenu));
      reuse.waitForElementVisible(rpObjects.getElement(DBLabelHeading));
      if ((reuse.isElementPresent(rpObjects.getElement(DBLabelHeading)))
          && (reuse.isElementPresent(rpObjects.getElement(DBLinkLogout)))) {
        logger.info("Successfully clicked on the Dashboard menu.");
      }
    } catch (Exception Ex) {
      logger.error("Exception occurred while clicking on the Withdraw menu: " + Ex.getMessage());
    }
  }

  public boolean loginToApplication(String Username, String Password) {
    boolean flag = false;
    try {
      logger.info("Logging into application.");
      Reporter.log("Enter User Name");
      reuse.enterText(rpObjects.getElement(loginUsername), Username);
      Reporter.log("Enter Password");
      reuse.enterText(rpObjects.getElement(loginPassword), Password);
      Reporter.log("Click Submit");
      reuse.click(rpObjects.getElement(loginSubmit));
      Reporter.log("Verify Dashboard UI");
      reuse.click(rpObjects.getElement(dashboardMenu));
      reuse.waitForElementVisible(rpObjects.getElement(DBLabelHeading));
      flag =
          reuse.isElementPresent(rpObjects.getElement(DBLabelHeading))
              && reuse.isElementPresent(rpObjects.getElement(DBLinkLogout));
    } catch (Exception Ex) {
      logger.error("Exception occurred while logging into application: " + Ex.getMessage());
    }
    return flag;
  }

  public boolean loginToApplication(String Username, String Password, String secretKey) {
    boolean flag = false;
    try {
      logger.info("Logging into application.");
      Reporter.log("Enter User Name");
      reuse.enterText(rpObjects.getElement(loginUsername), Username);
      Reporter.log("Enter Password");
      reuse.enterText(rpObjects.getElement(loginPassword), Password);
      Reporter.log("Click Submit");
      reuse.click(rpObjects.getElement(loginSubmit));
      Reporter.log("Verify Dashboard UI");
      reuse.click(rpObjects.getElement(dashboardMenu));
      // if ((!secretKey.equalsIgnoreCase("NA"))) {
      //   if (reuse.isElementPresent(rpObjects.getElement(mfaVerificationCodeInput))) {
      //     reuse.enterText(
      //         rpObjects.getElement(mfaVerificationCodeInput), getTheTOTPCode(secretKey.toString()));
      //     reuse.click(rpObjects.getElement(mfaVerifyButton));
      //     reuse.waitForElementVisible(rpObjects.getElement(mfaSuccessLogin));
      //     reuse.waitForElementInvisible(rpObjects.getElement(mfaSuccessLogin));
      //     //Thread.sleep(3000);
      //   }
      // }
      reuse.waitForElementVisible(rpObjects.getElement(DBLabelHeading));
      flag =
          reuse.isElementPresent(rpObjects.getElement(DBLabelHeading))
              && reuse.isElementPresent(rpObjects.getElement(DBLinkLogout));
    } catch (Exception Ex) {
      logger.error("Exception occurred while logging into application: " + Ex.getMessage());
      throw new AssertionError("Unable to login the opeartion!", Ex);
    }

    return flag;
  }

  public boolean logOutFromApplication() {
    boolean flag = false;
    try {
      reuse.click(rpObjects.getElement(dashboardMenu));
      logger.info("Logout from the application.");
      Reporter.log("Click Logout");
      reuse.click(rpObjects.getElement(logout));
      Reporter.log("Verify Login Page");
      flag =
          reuse.isElementPresent(rpObjects.getElement(loginUsername))
              && reuse.isElementPresent(rpObjects.getElement(loginPassword));
    } catch (Exception Ex) {
      logger.error("Exception occurred while logging out from the application: " + Ex.getMessage());
    }
    return flag;
  }

  public String getTheTOTPCode(String secretKey) {
    String verificationCode = null;
    try {
      String otpKeyStr = secretKey;
      Totp totp = new Totp(otpKeyStr);
      verificationCode = totp.now();

      System.out.println("twoFactorCode code is" + verificationCode);
    } catch (Exception Ex) {
      logger.error("Unable to genearte the verification code: " + Ex.getMessage());
    }
    return verificationCode;
  }

  public String splitString(String str) {
    Reporter.log("Value is" + str);
    String result = null;
    try {
      if (str != null) {
        String[] values = str.split(" ");
        String temp = values[0];
        result = temp.replace(",", "");
        Reporter.log("Value after split is" + result);
      }
    } catch (Exception e) {
      logger.error("Unable to split the string: " + e.getMessage());
    }
    return result;
  }

  public double getBalance(String option) {
    double retrivedBalance = 0;
    try {
      switch (option.toLowerCase()) {
        case "pla":
          logger.info("Retrieving the PLA balance.");
          System.out.println(
              Double.parseDouble(
                  splitString(reuse.getTextByInnerText(rpObjects.getElement(PLABalance)))));
          retrivedBalance =
              Double.parseDouble(
                  splitString(reuse.getTextByInnerText(rpObjects.getElement(PLABalance))));
          break;

        case "plaf":
          logger.info("Retrieving the PLAF balance.");

          retrivedBalance =
              Double.parseDouble(
                  splitString(reuse.getTextByInnerText(rpObjects.getElement(PLAFBalance))));
          break;

        default:
          logger.info("Option is not defined.");
          break;
      }
    } catch (Exception e) {
      logger.error("Unable to retrive the balance: " + e.getMessage());
    }
    return retrivedBalance;
  }

  public void verifyTheBalance() {
    try {
      reuse.isElementPresent(rpObjects.getElement(DBLabelHeading));
      reuse.isElementPresent(rpObjects.getElement(DBLabelFree));
      reuse.isElementPresent(rpObjects.getElement(DBLabelPaid));
      reuse.assertTextMatch(
          reuse.getTextByInnerText(rpObjects.getElement(PLAFBalance)), "4,109,229.7 PLAF");

    } catch (Exception e) {
      logger.error("Unable to retrive the balance: " + e.getMessage());
    }
  }
}

