// package org.baaf.api.gmailData;


// import java.io.IOException;
// import java.io.InputStream;
// import java.io.InputStreamReader;
// import java.security.GeneralSecurityException;
// import java.util.Collections;
// import java.util.List;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import javax.mail.MessagingException;
// import com.google.api.client.auth.oauth2.Credential;
// import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
// import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
// import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
// import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
// import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
// import com.google.api.client.http.javanet.NetHttpTransport;
// import com.google.api.client.json.JsonFactory;
// import com.google.api.client.json.jackson2.JacksonFactory;
// //import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
// import com.google.api.client.util.store.FileDataStoreFactory;

// import com.google.api.services.gmail.GmailScopes;
// import com.google.api.services.gmail.model.Label;
// import com.google.api.services.gmail.model.ListLabelsResponse;
// import com.google.api.services.gmail.model.ListMessagesResponse;
// import com.google.api.services.gmail.model.Message;
// import com.google.appengine.repackaged.org.apache.commons.codec.binary.Base64;
// import com.google.api.services.gmail.Gmail;
// import com.google.api.services.gmail.model.ListLabelsResponse;

// public class GmailQuickstart {
//     private static final String APPLICATION_NAME = "Gmail API Java Quickstart";
//     private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
//     private static final String TOKENS_DIRECTORY_PATH = "tokens";

//     /**
//      * Global instance of the scopes required by this quick start.
//      * If modifying these scopes, delete your previously saved tokens/ folder.
//      */
//     private static final List<String> SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);
//     private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

//     /**
//      * Creates an authorized Credential object.
//      * @param HTTP_TRANSPORT The network HTTP Transport.
//      * @return An authorized Credential object.
//      * @throws IOException If the credentials.json file cannot be found.
//      */
//     private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
//         // Load client secrets.
//         InputStream in = GmailQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
//         GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

//         // Build flow and trigger user authorization request.
//         GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
//                 HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
//                 .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
//                 .setAccessType("offline")
//                 .build();
//         LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
//         return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
//     }

//     public static void main(String... args) throws IOException, GeneralSecurityException, MessagingException {
//         // Build a new authorized API client service.
//         final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
//         Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
//                 .setApplicationName(APPLICATION_NAME)
//                 .build();

//         String user = "me";
//         ListLabelsResponse listResponse = service.users().labels().list(user).execute();
//         List<Label> labels = listResponse.getLabels();
//         if (labels.isEmpty()) {
//             System.out.println("No labels found.");
//         } else {
//             System.out.println("Labels:");
//             for (Label label : labels) {
//                 System.out.printf("- %s\n", label.getName());
//             }
//         }
        
//         ListMessagesResponse listMessages = service.users().messages().list(user).execute();
//         List<Message> messages = listMessages.getMessages();
//         	Message message = messages.get(0);
//         	Message actualContent = service.users().messages().get(user, message.getId()).setFormat("full").execute();
//         	System.out.println(actualContent.toPrettyString());
//         	System.out.println(actualContent.getPayload().getParts());
//         	System.out.println("Reading message:: " + message.getId());
// 			System.out.println("Reading message:: " + message.getRaw());
//     }
    
//     public String getDecodedString(String base64String) {
//          byte[] emailBytes = Base64.decodeBase64(base64String);
//          return new String(emailBytes);
//     }
    
//     public String retrieveURL(String url) {
//     	Pattern pStartingPattern = Pattern.compile("https[\\\\W\\\\w/:a-zA-Z0-9\\-.?=]*[!\"]");//. represents single character
//     	Matcher mURLStart = pStartingPattern.matcher(url);  
//     	mURLStart.find(url.indexOf("30 minutes after beginning"));
//     	String strFinalURL = mURLStart.group().trim();
//     	return strFinalURL.substring(0, strFinalURL.length());
//     }
// }