package org.baaf.api.requests;

import java.util.HashMap;

import org.baaf.api.APIHeaders;
import org.baaf.api.APIMethods;
import org.baaf.api.APIResponse;
import org.baaf.api.sharables.Shareable;
import org.baaf.utilities.json.JsonHelper;

public class PUTRequest extends APIRequest {

    String strPayLoad;

    public PUTRequest(APIMethods amMethod, JsonHelper jsAPIDetails, Shareable shVariables) {
        super(amMethod, jsAPIDetails,shVariables);
    }

    @Override
    public APIResponse execute() {
        APIResponse arAPIResponse = new APIResponse(amMethod);
        arAPIResponse.setResponse(given().body(strPayLoad).put(strAPIPath));
        return arAPIResponse;
    }

    @Override
    public void prepareAPI(APIHeaders ahAPIHeader, HashMap<String, String> erDataSet) {
        strAPIPath = updateURIParams(strAPIPath, erDataSet);
        given().headers(createHeaders(erDataSet).getHeaderMap());
        strPayLoad = createRequestPayLoad(erDataSet);
    }
    
}