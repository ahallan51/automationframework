package org.baaf.execution;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.baaf.api.sharables.SharedValues;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.block8.framework.data.ExcelReader;
import com.block8.framework.reporting.ExtentManager;

public class APIEngine {

    public static boolean debug;
    LoadConfigurations lcConfigurations;
    String strRootFolder;
    ExcelReader erAPISuite, erDataFeeder;
    String strCurrentAPIName;
    String strTestDataFile;
    String strDataSheet;
    CreateAPI capiAPIRequest;
    private SharedValues svSharedValues;
    public static ExtentManager extManager = null;
    public static ExtentReports extent = null;
    public static ExtentTest extLogger = null;

    public static Logger logger = null;

    public APIEngine() {
        this.lcConfigurations = new LoadConfigurations();
        this.strRootFolder = lcConfigurations.getRootFolder();
        erAPISuite = ExcelReader.getInstance(strRootFolder + "/Settld.xlsx", "APIList");
        logger = Logger.getLogger(APIEngine.class.getName());
        logger.info("Root Folder : " + this.strRootFolder);
        svSharedValues = new SharedValues();
    }

    public void setupReporting(){
        try {
            extManager = new ExtentManager(extent, strRootFolder);
            extent = ExtentManager.getReporter();
        } catch (IOException e) {
            e.printStackTrace();
            extLogger.fail("Error occured while setting up reporting.");
            logger.error(("Error occured while setting up reporting.") + e.getMessage());
        }
    }

    public void executeAPITests() {
        logger.info("######################## Execution Starts ######################");
        String strAPIName = erAPISuite.getData("API_Name");
        String strJSONFile = strRootFolder + "/requesttemplates/" + strAPIName + ".json";
        System.out.println(strAPIName);
        strDataSheet = erAPISuite.getData("DataSheet");
        strTestDataFile = strRootFolder + "/TestData/" + erAPISuite.getData("DataFiles") + ".xlsx";
        erDataFeeder = ExcelReader.getInstance(strTestDataFile, strDataSheet);
        capiAPIRequest = new CreateAPI(strJSONFile, erDataFeeder.getFullDataset());
        
        logger.info("----------------  API Test " + strAPIName + " Begins -------------------");
        while (erDataFeeder.hasNext()) {
            try {
                extLogger = ExtentManager.getLogger(strAPIName);
                capiAPIRequest.loadAPI(erAPISuite.getData("URL"),svSharedValues);
                capiAPIRequest.executeAPI();
                ValidationResult result = capiAPIRequest.validateResponse();
                capiAPIRequest.extractSharableValues(svSharedValues);
                if(result.getFinalResult()){
                    APIEngine.extLogger.pass("Response Validated Succefully");
                }else{
                    APIEngine.extLogger.fail("Response Validated Failed \n" + result.getMessage() );
                }
                logger.info("---------------- API " + strAPIName + " Ends -------------------");
                erDataFeeder.readNextRow();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.error(e.getMessage());
                logger.error(e.getStackTrace());
                e.printStackTrace();
                extLogger.fail("Exception occored while validating the response");
                break;
            }
            extLogger.log(Status.INFO, svSharedValues.toString());
            extent.flush();
        }
    }
    
    public void start() {
    	while(erAPISuite.hasNext()) {
    		try {
    			executeAPITests();
				erAPISuite.readNextRow();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
    	}
    }
}