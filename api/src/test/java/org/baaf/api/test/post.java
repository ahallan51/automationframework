package org.baaf.api.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes.Name;

import com.block8.framework.data.ExcelReader;

import org.baaf.configurations.Configurations;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

public class post {
    ExcelReader eReader;
    public static String APIrootFolder = "";

    // @DataProvider(name = "dataforPost") // created a function for DP where we are
    // creating our data
    @Test
    public void dataforPost()// i want to return an object which is a 2 dimensional(rows and columns) object
    {
        System.out.println("abc");
        // final Object[][] data = new Object[2][3];// declared an object wih 3 rows and
        // 3 cols.

        // data[0][0] = "subina";
        // data[0][1] = "dev";
        // data[0][2] = "158";

        // data[1][0] = "thomas";
        // data[1][1] = "HR";
        // data[1][2] = "118";

        // //return data;

        // Provide as many data no need to specify the no of columns or rows
        // return new Object[][] { { "subina", "hr", "123" }, { "Henry", "dev", "345" }
        // };
        final Configurations conf = new Configurations("/test.properties");
        HashMap<String, String> hm = conf.hmAPIDetails;
        APIrootFolder = (String) hm.get("RootPath");
        // final ArrayList<String> apiList = new ArrayList<>();

        try {
            eReader = ExcelReader.getInstance(APIrootFolder + "/ResponseExcel.xlsx", "Sheet1");

            while (eReader.hasNext()) {
                String a = eReader.getData("First_name");
                System.out.println(a);
                String abc = eReader.getData("Last_name");
                System.out.println(abc);
                String b = eReader.getData("Age");
                System.out.println(b);
                eReader.readNextRow();
                // eReader.getFullDataset();

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
