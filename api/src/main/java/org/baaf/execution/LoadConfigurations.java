package org.baaf.execution;

import java.util.HashMap;

import org.baaf.configurations.Configurations;

public class LoadConfigurations {
    
    Configurations cnfFrameorkd;
    CreateAPI caAPI;
    HashMap<String, String> hmConfigurations;

    public LoadConfigurations(){
        cnfFrameorkd = new Configurations("/baseConfig.properties");
        // caAPI = new  CreateAPI();
        hmConfigurations = cnfFrameorkd.getHashMap();
    }

    public String getRootFolder(){
        return hmConfigurations.get("rootPath");
    }

    public String getTestType(){
        return hmConfigurations.get("TestType");
    }
}