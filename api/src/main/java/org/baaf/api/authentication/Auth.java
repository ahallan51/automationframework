package org.baaf.api.authentication;



import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class Auth {
	RequestSpecification request;

	public RequestSpecification encoderType(String encoding) {	
	              
	               return request.config(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig().encodeContentTypeAs(encoding, ContentType.TEXT)));
	             		
	}
	
	public Auth(RequestSpecification request) {
		this.request = request;
		
	}
}
