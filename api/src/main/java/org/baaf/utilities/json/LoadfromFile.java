package org.baaf.utilities.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.jayway.jsonpath.internal.JsonReader;

public class LoadfromFile {

    File fileJsonPath;
    JsonReader jsonReader;

    public LoadfromFile(File strFilePath) {
        this.fileJsonPath = strFilePath;
    }

    public void loadJSON() {
        jsonReader = new JsonReader();
        try {
            jsonReader.parse(new FileInputStream(fileJsonPath), "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void parseFromString(String strJsonContent){
        jsonReader.parse(strJsonContent);
    }

    public JsonReader getValue(){
        return jsonReader;
    }
}