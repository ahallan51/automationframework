package com.selenium.testcases;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.block8.framework.selenium.*;
import com.block8.uiengine.UIExecute;
import com.selenium.pages.LoginPage;
import com.block8.framework.reporting.ExtentManager;
import com.block8.framework.repositories.Repository;
import com.block8.framework.data.*;
import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;
import java.lang.reflect.Method;
import java.io.IOException;
import java.util.List;

public class ConfigTests extends TestBase {
  public static SoftAssert _softAssert;
  public static String testCaseName = null;
  public static ExtentTest extLogger = null;
  public static ExtentManager extManager = null;
  public static ExtentReports extent = null;
  public static Logger logger = null;

  ReusableFunctions reuse;
  LoginPage login;
  String strTestDataUPdate;
  ExcelReader xlrDataProvided;
  Repository rpObjects;
  String strRepositoryFileName;
  String strSheetName;

  public ConfigTests(String strTestDataUpdate, String strSheetName, String strRepositoryFileName)
      throws Exception {
    super();
    this.strTestDataUPdate = strTestDataUpdate;
    this.strRepositoryFileName = strRepositoryFileName;
    this.strSheetName = strSheetName;
    this.xlrDataProvided =
    		 ExcelReader.getInstance(
    				 "F://mystake//Automation//APIAutomation//TestAutomation//BAAF_Root//uia//data//TestData.xlsx",
    	            strSheetName);
    		
//        ExcelReader.getInstance(
//                UIExecute.strRootDataFiles + strTestDataUPdate,
//            strSheetName);
    
    rpObjects =
        Repository.getRepositoryInstance(
        		"F://mystake//Automation//APIAutomation//TestAutomation//BAAF_Root//uia//repositories//"
                + strRepositoryFileName);
    
//    rpObjects =
//        Repository.getRepositoryInstance(
//          UIExecute.strRepositories
//                + strRepositoryFileName);
  }

  @BeforeSuite
  public void initializeFrameWork() throws Exception {}

  /* @BeforeMethod
  public void driverInitialization1() throws IOException {
      initialization();
      reuse = new ReusableFunctions(driver, prop);
  	login = new LoginPage(driver, prop,rpObjects);

  }*/

  @BeforeMethod
  public void setUp(Method method) throws IOException {
    _softAssert = new SoftAssert();
    testCaseName = method.getName();
    extManager = new ExtentManager(extent,UIExecute.strRootPath);
    extent = ExtentManager.getReporter();
    extLogger = ExtentManager.getLogger(testCaseName);
    logger = Logger.getLogger(method.getDeclaringClass());
    logger.info("################### Test Case Started: " + testCaseName + " ###################");
  }

  @BeforeClass
  public void driverInitialization() throws IOException {
    initialization();
    reuse = new ReusableFunctions(driver, prop);
    login = new LoginPage(driver, prop, rpObjects);
  }

  public String getData(String strFieldName) throws Exception {
    return xlrDataProvided.getData(strFieldName);
  }

  @AfterMethod
  public void getResult(ITestResult result) throws Exception {
    List<String> messages = Reporter.getOutput();
    for (String string : messages) {
      extLogger.log(Status.PASS, string);
    }
    if (result.getStatus() == ITestResult.FAILURE) {
      String screenShotPath = reuse.takeScreenShot(result.getTestName());
      extLogger.log(
          Status.FAIL,
          MarkupHelper.createLabel(
              result.getName() + " Test case FAILED due to below issues:", ExtentColor.RED));
      extLogger.fail((result.getThrowable()).getMessage());
      extLogger.info("Snapshot below: ");
      extLogger.addScreenCaptureFromPath(screenShotPath, testCaseName);
      //driver.quit();

    } else if (result.getStatus() == ITestResult.SKIP) {
      extLogger.log(
          Status.SKIP,
          MarkupHelper.createLabel(
              result.getName() + " Test case SKIPPED due to below issues:", ExtentColor.GREY));
      extLogger.skip((result.getThrowable()).getMessage());

    } else if (result.getStatus() == ITestResult.SUCCESS) {
      extLogger.log(
          Status.PASS,
          MarkupHelper.createLabel(result.getName() + " Test case PASSED.", ExtentColor.GREEN));
    }
    Reporter.clear();
    driver.manage().deleteAllCookies();
  }

  @AfterSuite
  public void driverTearDown() {
    extent.flush();
  }

  @AfterClass
  public void driverClose() {
    driver.quit();

  }
}
