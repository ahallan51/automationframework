package org.baaf.reporting;

import com.aventstack.extentreports.ExtentTest;

public class ValidationReporter {
	
	ExtentTest extReporter;
	
	public ValidationReporter(ExtentTest extReporter) {
		this.extReporter = extReporter;
	}
	
	public void addResults(String strMessage, Status stTestStatus) {
		switch (stTestStatus) {
		case PASS:
			extReporter.pass(strMessage);
			break;
		case FAIL:
			extReporter.fail(strMessage);
			break;
		case INFO:
			extReporter.info(strMessage);
			break;
		default:
			extReporter.warning(strMessage);
			break;
			
		}
	}
	
	public void assertResultsMatch(String strName, String strActualResult, String strExpectedResult) {
		if(strActualResult.equals(strExpectedResult)) {
			addResults( strName + " - Expected [" + strExpectedResult + "] Found [" + strActualResult + "].",
					Status.PASS);
		}else {
			addResults( strName + " - Expected [" + strExpectedResult + "] but Found [" + strActualResult + "].",
					Status.FAIL);
		}
	}
}
