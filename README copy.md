# Playchip Test Automation Framework

### Framework Contents:
- Page Object Model
- Extent Reporting
- Data Driven (Apache POI)
- Control test execution using run mode
- Maven Project (No external jar files needs to be added)
- Log4j
- RetryAnalyzer for automatic test execution of failed cases

### Initial Setup: 
- Install and configure [JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
- Install and configure [Maven](https://maven.apache.org/download.cgi)
- Setup your IDE (Preferably [IntelliJ IDEA](https://www.jetbrains.com/idea/download/#section=windows))
- Import cloned repository as project
- Add your test case classes under testcases package
- Add your test data and run mode settings to TestData.xslx file
- Update testng.xml file 
- Simply run  *mvn clean install* command to run your test cases

### Introduction

This document is an operational guideline for running our test automation framework for PlayChip Smoke testing suite.

### Environments

Selenium automation scripts with docker using CircleCI by CI/CD pipeline.
Automation Environment Setup
The automation environment requires installation of followings in the execution machine.
 1. Java
 2. Maven
 3. TestNG
 4. Github (Source Code control)
 5. Please update the credentials_template.json file with valid data and rename the file to credentials.json in the same location.
 
### Automation framework deployment

1.Connect to the Github and pull the latest code.

2.Go to the TestAutomation\SeleniumAutomation folder.

3.If required update the values of browser & URL in the config.properties file.

  The file should be available in the TestAutomation\SeleniumAutomation\src\main\resource folder.
  
  (Note: Currently we have only tested with chrome browser.)
  
4.Once done with the setup. To execute specific test case of a test file. 

5.Go to the command prompt and navigate to the selenium automation folder in the command prompt and execute the below command.

    mvn test -Dtest=Testclassname#testmethodname test 
    ex as mvn test -Dtest=SupportLinkTest#verifySupportLink test.
6.To execute the whole suite. Go to the command prompt and execute below command as per your requirement.

      mvn test (if you don't want to compile)
      mvn clean install (it will recompile  and execute all the tests.)
7. To update the element details in the framework due to the CSS change in the application. Please follow the steps accordingly.

    a.Identify the element details (like id, xpath etc)  and the page.
    
    b.File should be available at TestAutomation\SeleniumAutomation\src\main\resource\Repository\ PlayChipCentralRepo.txt.
    
    c.Open the file and do the changes for the respective page and save it. Don�t change the names only the xpath or ID details should change.
    
###Evaluate Results

At the completion of your tests, an html report should be created for the executions with the detailed summary of tests passed/failed. You can evaluate the results in the mentioned path
SeleniumAutomation\ExtentReports .Please refers the latest report in the folder.
