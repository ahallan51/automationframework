package com.block8.framework.selenium;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MouseActions {

  WebDriver driver;
  Actions aActions;

  public MouseActions(WebDriver driver) {
    this.driver = driver;
    aActions = new Actions(driver);
  }

  public boolean hoverOverElement(WebElement element) {

    try {
      WebDriverWait wwWaitForElement = new WebDriverWait(driver, 10);
      wwWaitForElement.until(ExpectedConditions.visibilityOfAllElements(element));
      aActions.moveToElement(element).perform();
    } catch (ElementNotVisibleException e) {
      e.printStackTrace();
    }
    return true;
  }

  public boolean hoverAndClick(WebElement element) {
    hoverOverElement(element);
    aActions.clickAndHold(element);
    return true;
  }

  public boolean dragElement(WebElement sourceElement, WebElement targetElement) {
    aActions.dragAndDrop(sourceElement, targetElement);
    return true;
  }
}
