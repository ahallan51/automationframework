package org.baaf.configurations.test;

import java.util.HashMap;
import java.util.Set;

import org.baaf.configurations.Configurations;
import org.testng.annotations.Test;

@Test
public class ConfigurationsTest{

    @Test
    public void testConfigurations(){
        Configurations conf = new Configurations("/test.properties");
        HashMap<String, String> hm = conf.getHashMap();
        Set<String> keys = hm.keySet();
        for (Object object : keys) {
            System.out.println("Key - " + (String)object + " Value - " + hm.get(object));   
        }
    }
}