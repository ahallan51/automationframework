package org.baaf.utilities.patterns;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.baaf.execution.APIEngine;

public class PatternMatcher {

    Pattern pPattern;
    Matcher matches;
    int iSandStart=0,iSandEnd=0;
    String strFilter;
    HashMap<String,String> hmPatterns;

    public PatternMatcher(String strPatternString,int iSandStart,int iSandEnd){
        pPattern = Pattern.compile(strPatternString);
        this.iSandEnd = iSandEnd;
        this.iSandStart = iSandStart;
        hmPatterns = new HashMap<>();
    }

    public void setFilterString(String strFilter){
        this.strFilter = strFilter;
        matches = pPattern.matcher(strFilter);
    }

    public HashMap<String,String> getPatterns(){
        return hmPatterns;
    }

    public void getMatchingIndexes(){
        while(matches.find()){
        if(APIEngine.debug == true)
        {
            System.out.println(matches.groupCount());
            System.out.println(matches.group());
            System.out.println("Start " + matches.start() + " End " + matches.end());
        }
            String paramName = strFilter.substring(matches.start()+iSandStart,matches.end()-iSandEnd);
            String Pattern = strFilter.substring(matches.start(),matches.end());
            // System.out.println("String to be replaced " + paramName + " Group :: " + Pattern);
            hmPatterns.put(paramName, Pattern);
        }
    }


    public String getStringFromPattern(){
        if(APIEngine.debug == true)
        {
            System.out.println("Start " + matches.start() + " End " + matches.end());
            System.out.println("String to be replaced " + strFilter.substring(matches.start()+iSandStart,matches.end()-iSandEnd));;
        }
        String strFoundString = strFilter.substring(matches.start()+iSandStart,matches.end()-iSandEnd);
        return strFoundString;
    }

    public boolean hasMoreMatches(){
        return matches.find();
    }

    public boolean isMatching(String strCheckPattern){
        return pPattern.matcher(strCheckPattern).groupCount()>0;
    }
    
}