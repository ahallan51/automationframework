package org.baaf.api.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.*;

public class dataprovidertests {

    @Test
    @DataProvider(name = "dataforPost") // created a function for DP where we arecreating our data
    public Object[][] dataforPost()// i want to return an object which is a 2 dimensional(rows and columns) object
    {
        Object[][] data = new Object[2][3];// declared an object wih 3 rows and 3cols.

        data[0][0] = "subina";
        data[0][1] = "dev";
        data[0][2] = "158";

        data[1][0] = "thomas";
        data[1][1] = "HR";
        data[1][2] = "118";

        return data;

        // // Provide as many data no need to specify the no of columns or rows
        // return new Object[][] { { "subina", "hr", "123" }, { "Henry", "dev", "345" }
        // };

    }

}